
version in ThisBuild := "0.1.0-SNAPSHOT"

scalaVersion in ThisBuild := "2.11.12"

lazy val root = (project in file("."))
  .settings(
    name := "bootstrap"
  )

libraryDependencies ++= Seq( "org.apache.spark" %% "spark-core" % "2.4.0" % "provided",
  "org.apache.spark" %% "spark-sql" % "2.4.0" % "provided",
  "org.apache.spark" %% "spark-hive" % "2.4.0" % "provided",
  "com.typesafe" % "config" % "1.4.2",
  "javax.mail" % "mail" % "1.4.7",
  "org.slf4j" % "slf4j-api" % "1.7.6" % "provided",
  "org.apache.httpcomponents" % "httpclient" % "4.3.6" % "provided",
  "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.4.4" % "provided",
  "com.databricks" % "spark-csv_2.11" % "1.0.3",
  "org.joda" % "joda-convert" % "1.8.1")



assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}


assemblyMergeStrategy in assembly := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x => MergeStrategy.first
}



