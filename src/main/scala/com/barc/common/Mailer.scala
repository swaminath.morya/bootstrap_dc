package com.barc.common



import javax.mail.Message
import javax.mail.{Authenticator, Message, MessagingException, PasswordAuthentication, Session, Transport}
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage
import com.typesafe.config.{Config, ConfigFactory, ConfigList}

import java.io.File
import scala.collection.JavaConverters.asScalaBufferConverter
/*
* Code: Mailer main object
* About: It is used to send mail
* Developer: Big data Team (Swaminath M)
* Date: 30-May-2022
* */


object Mailer {
  def sendMail(sub:String, content:String) =
  {
    val config=ConfigFactory.parseFile(new File("./email.conf"))
    //val config: Config = ConfigFactory.load("email.conf")
    val to:List[String]   = config.getStringList("email.receipt").asScala.toList
    println(to)
    val from:String = config.getString("email.sender")
    val password:String = config.getString("email.pswd")
    //val to = "swaminath.morya@barcindia.co.in"
     //val from = "sparkalerts@barcindia.co.in"
     val host = "smtp.office365.com"
    val properties = System.getProperties
    properties.setProperty("mail.smtp.host", host)
    properties.setProperty("mail.smtp.port", "587")
    properties.setProperty("mail.smtp.auth", "true")
    properties.setProperty("mail.smtp.starttls.enable", "true")

    val session = Session.getInstance(properties, new Authenticator() {
      override protected def getPasswordAuthentication = new PasswordAuthentication(from, password)
    })
    // session.setDebug(true)
    try {
      val message = new MimeMessage(session)
      message.setFrom(new InternetAddress(from))
      println("to.toString",to.toString)
      for(element<-to)
      {
        println("Sending mail to : ",element)
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(element))
      }

      message.setSubject(sub,"utf-8")
      //message.setText(content, "text/html")
      message.setContent(content, "text/html; charset=utf-8")
      System.out.println("sending...")
      Transport.send(message)
      System.out.println("Sent message successfully....")
    } catch {
      case mex: MessagingException =>
        mex.printStackTrace()
    }
  }
}