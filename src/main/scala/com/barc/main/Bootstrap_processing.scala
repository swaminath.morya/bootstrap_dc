package com.barc.main

import com.barc.common.Mailer.sendMail
import com.barc.common.raise_Info
import com.barc.main.get_ami._
import com.barc.main.get_ami_perf.get_ami_proc_pref

import java.time.Duration
import scala.Array.ofDim
//import com.barc.main.mainApi.{spark, _}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{DataFrame, SparkSession}
//import com.typesafe.config.Config
import java.time.LocalDateTime
/*
* Code: get_ami() main object
* About: This is main api of bootstrap process. Which load data from config and load data to process and generate final result
*        It is redeveloped from postgres procedure to Spark scala code
* Developer: Big data Team (Swaminath M, Monas C)
* Date: 3-Apr-2022
* */
object Bootstrap_processing extends App {
  //System.setProperty("hadoop.home.dir", "C:\\winutils")
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  Logger.getLogger("info").setLevel(Level.OFF)
  Logger.getLogger("INFO").setLevel(Level.OFF)
  val spark = SparkSession.builder().appName("BootstrapAPI").enableHiveSupport().getOrCreate()
  println("Welcome user:",scala.util.Properties.userName)
  if (args.length < 6) {
    raise_Info("Bootstrap_processing", "INFO", "31", s"[${LocalDateTime.now()}][hh_vhr_generator] === Kindly pass 5 arguments 5/${args.length} passed" +
      s" \n ****************************************** " +
      s" \n p_config_filename : Bootstrap_request_Phase1.csv {in hdfs location}" +
      s" \n p_bootstrap_loop : {0-100}" +
      s" \n p_household_filename :  table_name " +
      s" \n p_vhr_filename : table_name " +
      s" \n v_pre_job_id : Jobid to rerun on previous data again. Else 0  " +
      s" \n  input_data_load : Load data from input file into table(Y/N)" +
      s" \n ***************Job exit -Rerun job with given parameter****************************")
    sys.exit()
  }
  var p_config_filename = args(0) //"2020-01-04"
  var p_bootstrap_loop = args(1).toInt //50
  var v_valid_loop_count: Int = p_bootstrap_loop;
  var v_household_tblname = args(2)
  var v_load_vhr_tblname = args(3)
  var v_pre_job_id =args(4).toInt
  var input_data_load=args(5)
  //var v_group_id=args(6)


  raise_Info("Bootstrap_processing", "INFO", "29", s"[${LocalDateTime.now()}][Bootstrap_processing]  =====check and create if not exists Database========")
  spark.sql("create database if not exists bootstrap")
  spark.sql("use bootstrap")
  spark.catalog.clearCache()
  spark.sql("SET hive.exec.dynamic.partition = true")
  spark.sql("SET hive.exec.dynamic.partition.mode = nonstrict ")
  spark.sql("SET hive.exec.max.dynamic.partitions.pernode = 400")
  spark.conf.set("spark.sql.shuffle.partitions",400)
//  spark.conf.set("spark.sql.codegen.wholeStage", false)
  spark.conf.set("spark.speculation","false")
 // spark.sql("Create table if not exists bootstrap_job (job_id int, job_date string, job_status string )")

  //var job_id = spark.sql("select nvl(max(job_id),0)+1 job_id from bootstrap_job ").head().get(0).toString.toInt

  if (input_data_load == "Y") {
    raise_Info("Bootstrap_processing", "INFO", "29", s"[${LocalDateTime.now()}][Bootstrap_processing]  =====Loading data from Input file========")
    var a = spark.read.option("header", "true").option("inferSchema", "true").csv(s"/user/linconnect/Inputs/RAW/${v_household_tblname}.csv")

    a.write.mode(org.apache.spark.sql.SaveMode.Overwrite).format("parquet").saveAsTable(s"${v_household_tblname}")

    var b = spark.read.option("header", "true").option("inferSchema", "true").csv(s"/user/linconnect/Inputs/RAW/${v_load_vhr_tblname}.csv")

    b.write.mode(org.apache.spark.sql.SaveMode.Overwrite).format("parquet").saveAsTable(s"${v_load_vhr_tblname}")
  }

  var job_id = 1
  try { job_id = spark.sql("select nvl(max(job_id),0)+1 job_id from bootstrap_job ").head().get(0).toString.toInt} catch {case ex: Exception => var job_id: Int = 1  }
  var job_check = 0

  try {   job_check = spark.sql(s"select count(1) from bootstrap_job where job_id=${job_id - 1}  ").head().get(0).toString.toInt} catch {  case ex: Exception => var job_check: Int = 0}
  if (job_check == 1) {
    raise_Info("Bootstrap_processing", "INFO", "29", s"[${LocalDateTime.now()}][Bootstrap_processing]  ===== Job Exit Since Jobid: ${job_id - 1} is running========")
   // sys.exit()
  }

  var v_status = "Success"
  var error_msg =""
  var job_date = LocalDateTime.now()
  var v_job_df =spark.sql(s"select $job_id job_id,'$v_household_tblname' job_hh_table,'$v_load_vhr_tblname' job_vhr_table, '$p_config_filename' config_file_name,'$job_date' job_date ,'Running' job_status")
  v_job_df.write.mode("append").saveAsTable("bootstrap_job")


 // spark.sql(s"insert into bootstrap_job values ($job_id,'$job_date' ,'Running') ")
  raise_Info("Bootstrap_processing", "INFO", "29", s"[${LocalDateTime.now()}][Bootstrap_processing]  ======================= start Bootstrap_processing()===================")

  //--------------------Module 1: Data and Parameter file Loading--------------------
  var i: Int = 0;
  var n: Int = 0;
  var v_bootstrap_id: Int = 0;
  var v_total_bootstrap_id: Int = 0;
  var v_Full_Sample_AVGIMP = 0.0;
  var v_avg_BSAVGIMP = 0.0;
  var v_avg_bsrating = 0.0;
  var v_std_error_BSRATING = 0.0;

  var v_avg_bsu = 0.0;

  // var p_start_date = "2020-01-04";

  //( cast(b.tp_date as date) >= (date_format('$p_start_date','YYYY-MM-DD')) AND cast(b.tp_date as date) < (date_add(date_format('$p_start_date','YYYY-MM-DD'), $v_repeat_days)) )
  var p_start_date ="2020-01-04"
  var p_age_range = ofDim[Int](2, 5)
  p_age_range(0)(0) = 1
  p_age_range(0)(1) = 7
  p_age_range(0)(2) = 18
  p_age_range(0)(3) = 35
  p_age_range(0)(4) = 55
  p_age_range(1)(0) = 6
  p_age_range(1)(1) = 17
  p_age_range(1)(2) = 34
  p_age_range(1)(3) = 54
  p_age_range(1)(4) = 120

  var v_age_range = ofDim[Int](2, 5)
  v_age_range(0)(0) = 1
  v_age_range(0)(1) = 7
  v_age_range(0)(2) = 18
  v_age_range(0)(3) = 35
  v_age_range(0)(4) = 55
  v_age_range(1)(0) = 6
  v_age_range(1)(1) = 17
  v_age_range(1)(2) = 34
  v_age_range(1)(3) = 54
  v_age_range(1)(4) = 120

  var v_total_vhr_input: Int = 0;
  var v_total_vhr_orig: Int = 0;
  var v_member_universe_count: Int = 0;
  var v_count_1: Int = 0;
  var v_count_2: Int = 0;
  var v_sql_statement: String = "";
  var items: DataFrame = spark.emptyDataFrame
  var p_sample_size = 0
  var p_default_bootstrap_loop = 50

  var p_repeat_days = 7;
  var p_repeat_weeks = 1;
  var p_gender = 0;
  var p_age_low = 1;
  var p_age_high = 120;
  var p_session_id = 0
  var p_tvset_id = 0;
  var p_tp_end = 86400;
  var p_tp_begin = 0;
  var v_Statement = ""


  var final_bs_df = spark.emptyDataFrame
  var input_file = spark.read.format("csv").option("header", "true").load(s"./Inputs/$p_config_filename")

  //  spark.sql("drop table  IF  EXISTS bootstrap_final_result")
 /* v_Statement =
    s""" CREATE  TABLE IF NOT EXISTS bootstrap_final_result(
                    job_id String,
                    group_id String,
                    avg_bsrating  decimal(30,10),
                    std_error decimal(30,10),
                    avg_bsavgimp decimal(30,10),
                    full_sample_avgimp decimal(30,10),
                    avg_bsu decimal(30,10),
                    valid_loop_count Int,
                    input_loop_count Int )"""
  spark.sql(v_Statement)*/
  var group_id: String = "0"

  raise_Info("Bootstrap_processing", "INFO", "127", s"[${LocalDateTime.now()}][Bootstrap_processing]  ======================= Loop GroupID()===================")

  var stg = spark.read
    .format("csv")
    .option("header", "true")
    .option("delimiter", ",")
    .option("inferSchema", "true")
    .load(s"./Inputs/stategroup.csv").toDF()
  stg.write.mode("Overwrite").saveAsTable("stg")

  var f_cntr=0
  //input_file.select("*").filter(s"group_id == $v_group_id").collect().foreach { data =>
  input_file.select("*").collect().foreach { data =>
    try {
      spark.catalog.clearCache()
      var input_Segregation1 = input_file.filter(col("Group_ID").===(data.get(0))).select("state_group", "detailed_nccs", "detailed_town_class")
      input_Segregation1 = input_Segregation1.withColumnRenamed("state_group", "state").withColumnRenamed("detailed_nccs", "nccs")
        .withColumnRenamed("detailed_town_class", "townvillagecode")
      println("input_Segregation1: Loop no" + data.get(0))

      try { group_id = data.get(0).toString()} catch {case ex: Exception => var group_id: Int = 0;}
      try {  v_bootstrap_id = data.get(1).toString().toInt} catch { case ex: Exception => var v_bootstrap_id: Int = 0;}
      try {   p_gender = data.get(2).toString().toInt} catch { case ex: Exception => var p_gender: Int = 0;}
      try {p_age_low = data.get(3).toString().toInt} catch { case ex: Exception => var p_age_low: Int = 1;}
      try { p_age_high = data.get(4).toString().toInt} catch { case ex: Exception => var p_age_high: Int = 120;}
      try { p_session_id = data.get(8).toString().toInt} catch {case ex: Exception => var p_session_id: Int = 0;}
      try { p_tvset_id = data.get(9).toString().toInt} catch { case ex: Exception => var p_tvset_id: Int = 0;}
      try { p_tp_begin = data.get(10).toString().toInt} catch { case ex: Exception => var p_tp_begin: Int = 0;}
      try { p_tp_end = data.get(11).toString().toInt} catch { case ex: Exception => var p_tp_end: Int = 86400;}
      try { p_start_date = data.get(12).toString()} catch { case ex: Exception => var p_start_date: Int = 0;}
      try { p_repeat_days = data.get(13).toString().toInt} catch {case ex: Exception => var p_repeat_days: Int = 7;}
      try {  p_repeat_weeks = data.get(14).toString().toInt } catch {case ex: Exception => var p_repeat_weeks: Int = 1;}
      try { p_sample_size = data.get(15).toString().toInt } catch {case ex: Exception => var p_sample_size: Int = 0; }
      try { p_default_bootstrap_loop = data.get(16).toString().toInt } catch {case ex: Exception => var p_default_bootstrap_loop: Int = 5;}

      println("Printing input_Segregation1: ", v_bootstrap_id, p_gender, p_age_low, p_age_high, p_session_id, p_tvset_id, p_tp_begin, p_tp_end, p_start_date, p_repeat_days, p_repeat_weeks, p_sample_size, p_bootstrap_loop)

      raise_Info("Bootstrap_processing", "INFO", "136", s"${input_Segregation1.show()}")

      var v_sql_date_range =""
      println("Inside p_repeat_weeks",p_repeat_weeks)
     /* v_sql_date_range = '( b.tp_date >= (to_date(''' || p_start_date || ''',''YYYY-MM-DD''))
      AND b.tp_date < ((to_date(''' || p_start_date || ''',''YYYY-MM-DD'')) + ' || p_repeat_days || ')) ';*/
     val b_tpdate="from_unixtime(unix_timestamp(b.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd')"
      val pstart_date=s"from_unixtime(unix_timestamp('$p_start_date', 'yyyy-MM-dd'), 'yyyy-MM-dd')"

      v_sql_date_range =  s""" ( from_unixtime(unix_timestamp(b.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd') >=  from_unixtime(unix_timestamp('$p_start_date', 'yyyy-MM-dd'), 'yyyy-MM-dd')
                    and  from_unixtime(unix_timestamp(b.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd') < date_add( from_unixtime(unix_timestamp('$p_start_date', 'yyyy-MM-dd'), 'yyyy-MM-dd'),$p_repeat_days)) """


      if (p_repeat_weeks.toInt >1) {
        /*  IF p_repeat_weeks > 1 THEN
          FOR i IN 2 .. p_repeat_weeks LOOP
          v_sql_date_range = v_sql_date_range || 'OR (b.tp_date >= (to_date(''' || p_start_date || ''',''YYYY-MM-DD'') + ' || (i-1) * 7 || ')
        AND b.tp_date < ((to_date(''' || p_start_date || ''',''YYYY-MM-DD'')) + ' || p_repeat_days + (i-1) * 7 || ')) ';
        END LOOP;
        END IF;*/

        /* v_sql_date_range =
          s"""  ( from_unixtime(unix_timestamp(b.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd')
                           >=  from_unixtime(unix_timestamp('$p_start_date', 'yyyy-MM-dd'), 'yyyy-MM-dd')
                    and  from_unixtime(unix_timestamp(b.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd')
                     <= date_add( from_unixtime(unix_timestamp('$p_start_date', 'yyyy-MM-dd'), 'yyyy-MM-dd'),${p_repeat_days.toInt} * ${p_repeat_weeks.toInt}) )
                     """*/
        for (i <- 2 to p_repeat_weeks) {
          v_sql_date_range =
            s""" $v_sql_date_range
               or ($b_tpdate >= date_add($pstart_date,($i-1)*7 ) and  $b_tpdate <  date_add($pstart_date,${p_repeat_days.toInt} +($i-1) *7 ))  """
        }
      }
      println("v_sql_date_range :",v_sql_date_range)

      input_Segregation1.createOrReplaceTempView("input_preprocess_tmp")

      var input_Segregation2 = input_file.filter(col("Group_ID").===(data.get(0))).select("bootstrap_id", "sex", "age_low", "age_high", "session_id", "tvset_id", "tp_start", "tp_end", "start_date", "repeat_days", "repeat_weeks", "sample_size", "bootstrap_count")
      input_Segregation2 = input_Segregation2.withColumnRenamed("sex", "gender").withColumnRenamed("tp_start", "tp_begin")

      raise_Info("Bootstrap_processing", "INFO", "143", s"[${LocalDateTime.now()}][Bootstrap_processing] Running Group_id: ${data.get(0)}")
      raise_Info("Bootstrap_processing", "INFO", "144", s" ${input_Segregation2.show()}")

      input_Segregation2.createOrReplaceTempView("input_parameter_tmp")

      //Write the code to import multiple hh_member_*.csv files to member_universe_input table
      //Current code is only for 1 hh_member file

      raise_Info("Bootstrap_processing", "INFO", "163", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  ======================= loading input_preprocess===================")

      var input_preprocess = spark.sql(
        s"""
			SELECT  t2.state, LOWER(TRIM(t2.nccs)) nccs, explode(split(coalesce(t2.townvillagecode,'%'),'[|]')) as townvillagecode
			FROM 	(SELECT t1.state, explode(split(coalesce(t1.nccs,'%'),'[|]')) AS nccs,t1.townvillagecode
					FROM	(SELECT  explode(split(coalesce(state,0),'[|]')) AS state, nccs, townvillagecode
								FROM input_preprocess_tmp	) t1
					) t2
	""".stripMargin)
      input_preprocess.write.mode("overwrite").saveAsTable("input_preprocess")
      raise_Info("Bootstrap_processing", "INFO", "175", s"input_preprocess \n ${input_preprocess.show()}")
      //  exportToHive(input_preprocess, List(), "", s"input_preprocess_$in_date", "default", hiveContext)

      raise_Info("Bootstrap_processing", "INFO", "178", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  ======================= loading input_parameter()===================")

      //changed split(session_id, '[|]') session_id  --to--
      var input_parameter = spark.sql(
        s"""(SELECT $job_id job_id,$group_id group_id,bootstrap_id as id,bootstrap_id,gender,age_low,age_high,nvl(session_id,'0') session_id,
					   tvset_id,tp_begin,tp_end,start_date,repeat_days,repeat_weeks,sample_size,bootstrap_count
		        	FROM input_parameter_tmp)
	       """.stripMargin)
      input_parameter.write.mode("append").saveAsTable("input_parameter")
      raise_Info("Bootstrap_processing", "INFO", "188", s"input_parameter \n ${input_parameter.show()}")

      raise_Info("Bootstrap_processing", "INFO", "190", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  ====================== Fetching id from input_parameter===================")

      v_total_bootstrap_id = spark.sql(s"SELECT max(group_id) FROM input_parameter where job_id=$job_id ").head().get(0).toString.toInt

      raise_Info("Bootstrap_processing", "INFO", "194", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  ======================= loading member_universe()===================")



      if (f_cntr==0 ) {
        f_cntr = f_cntr+1

        //Loading member universe
        val query =s"""
                  SELECT $job_id job_id ,a.state, LOWER(TRIM(replace(a.nccs,'"',''))) as nccs, LOWER(TRIM(replace(a.townvillagecode,'"',''))) as townvillagecode,
                          a.household_id, a.member_id,round(a.gender) gender, round(a.age) age, replace(a.date,'"','')  as tp_date, a.Weights as weight
                  FROM $v_household_tblname  a   join input_preprocess b  on (a.state = b.state)
                  WHERE a.household_id IS NOT NULL
                  AND (b.nccs = '%' OR LOWER(TRIM(replace(a.nccs,'"',''))) like  LOWER(TRIM(b.nccs)) )
                  and (b.townvillagecode = '%'  OR   LOWER(TRIM(replace(a.townvillagecode,'"',''))) like LOWER(TRIM(b.townvillagecode)))
                  ORDER BY a.state, a.date, a.household_id, a.member_id
              """.stripMargin

        raise_Info("Bootstrap_processing", "INFO", "207", s"member_universe:\n$query")

        var member_universe = spark.sql(query)
        raise_Info("Bootstrap_processing", "INFO", "207", s"member_universe: \n ${member_universe.show(10)}")

        member_universe.coalesce(3).write.partitionBy("job_id", "state").mode("append").saveAsTable("member_universe")
        //
        //------------------------Module 4: Create viewer viewing records using VHR and Member_universe--------------------
        raise_Info("Bootstrap_processing", "INFO", "211", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  ======================= loading viewer_viewing_records_orig_tmp()===================")
        if ( v_pre_job_id==0)
        {
           stg.select("*").orderBy("state_group").collect().par.foreach { data =>
          try {
            var state_group = data.get(0)
            raise_Info("Bootstrap_processing", "INFO", "238", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  Loading viewer_viewing_records_orig() - stategroup :  $state_group")
             val vv_qry=s""" SELECT b.job_id,  a.week, a.household_id, a.member_id, LOWER(TRIM(a.detailed_nccs)) nccs,
                   round(sex) as gender,  round(age_in_years) age, a.tvset_id,
                 a.session_id as session, a.begin as tp_begin, a.end as tp_end,
                 a.date tp_date,
                 a.state_group as state, LOWER(TRIM(a.detailed_town_class)) as townvillagecode, b.weight
                FROM $v_load_vhr_tblname a, member_universe b
                WHERE
                 from_unixtime(unix_timestamp(a.date, 'yyyy-MM-dd'), 'yyyy-MM-dd')=from_unixtime(unix_timestamp(b.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd')
                  AND a.household_id=b.household_id
                  AND a.member_id=b.member_id
                  AND a.sex=b.gender
                  AND a.age_in_years=b.age
                  AND a.state_group=b.state
                  AND LOWER(TRIM(a.detailed_nccs))=b.nccs
                  AND LOWER(TRIM(a.detailed_town_class))=b.townvillagecode
                  and b.job_id=$job_id
                  and a.state_group = $state_group
                  """.stripMargin
           //  println( "viewer_viewing_records_orig",vv_qry)
            var viewer_viewing_records_orig = spark.sql(vv_qry)
            //ORDER BY a.tp_date, a.household_id, a.member_id, a.gender, a.age, a.state, a.nccs, a.townvillagecode
           synchronized {
             viewer_viewing_records_orig.repartition(4).write.partitionBy("job_id", "state").mode("append").saveAsTable("viewer_viewing_records_orig")
           }
            raise_Info("Bootstrap_processing", "INFO", "269", s"[${LocalDateTime.now()}]viewer_viewing_records_orig  Loaded for Stategroup -$state_group")
          }
          catch {
            case ex: Exception => println(ex);
              v_status = "Error -3"
              error_msg =ex.toString
          }
        }
        }
      } //f_cntr closing
      //------------------------Module 5: Build Primary control universe--------------------
      //--------------------(% distribution of HH within State-NCCS-TC combo)--------------------

      raise_Info("Bootstrap_processing", "INFO", "274", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  ######## Step 10: Build post_stratify_universe table")

      build_post_stratify_universe(spark: SparkSession, p_age_range, group_id: String, job_id: Int)

      raise_Info("Bootstrap_processing", "INFO", "277", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  ======================= processing  build_universe_primary_control()===================")

      build_universe_primary_control(spark: SparkSession, group_id: String, job_id: Int)

      raise_Info("Bootstrap_processing", "INFO", "284", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  ======================= Looping $v_total_bootstrap_id based on input_parameter===================")
      try {
        // Group id in config max is 10
        // for (i <- 1 to v_total_bootstrap_id) {

        raise_Info("Bootstrap_processing", "INFO", "316", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  ===== Start Input Filter : $i=, $v_bootstrap_id : % ===========================")
        raise_Info("Bootstrap_processing", "INFO", "317", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  ===== parameter : $i=,  $v_bootstrap_id,$p_gender,$p_age_low,$p_age_high,$p_session_id,$p_tvset_id,$p_tp_begin,$p_tp_end,$p_start_date,$p_repeat_days,$p_repeat_weeks,$p_sample_size,$p_bootstrap_loop ===========================")

        if (p_sample_size < 0 || p_sample_size.isNaN) {
          raise_Info("Bootstrap_processing", "INFO", "320", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing] The current value for sample_size($p_sample_size) is invalid. Use value in table household_sample_size by date.")
          p_sample_size = 0;
        }

        if (p_default_bootstrap_loop < 2 || p_default_bootstrap_loop.isNaN) {

          raise_Info("Bootstrap_processing", "INFO", "326", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  The current value for bootstrap_count($p_bootstrap_loop) is <2 or NULL. Default to $p_default_bootstrap_loop.")
          p_bootstrap_loop = p_default_bootstrap_loop;

        }
        var start_time=LocalDateTime.now()
        raise_Info("Bootstrap_processing", "INFO", "329", s"[${start_time}][$group_id][Bootstrap_processing]  Calling get_ami_proc().")

        get_ami_proc_pref(spark: SparkSession, p_age_low: Int, p_gender: Int, p_age_high: Int,
          p_tp_begin: Int, p_tp_end: Int, p_repeat_days: Int, p_repeat_weeks: Int,
          p_bootstrap_loop: Int, p_age_range: Array[Array[Int]], p_start_date: String,
          p_sample_size: Int, p_session_id: Int, p_tvset_id: Int, v_sql_date_range: String, group_id: String, job_id: Int)
        v_Full_Sample_AVGIMP=0
        var total_dur =Duration.between(start_time, LocalDateTime.now()).toHours()
        raise_Info("Bootstrap_processing", "INFO", "336", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  End of  get_ami_proc(). Time taken $total_dur")
        v_Full_Sample_AVGIMP = spark.sql(s"SELECT avg(fsavgimp)   FROM bootstrap_result_fs where job_id=$job_id and group_id=$group_id limit 1").head().get(0).toString.toDouble

        v_valid_loop_count = spark.sql(s"SELECT count(*)   FROM bootstrap_result where job_id=$job_id and group_id=$group_id limit 1 ").head().get(0).toString.toInt

        if (v_valid_loop_count > 0) {
         /* spark.sql(
            s""" INSERT INTO bootstrap_final_result SELECT $job_id, $group_id ,avg(bsrating) v_avg_bsrating, stddev(bsrating) v_std_error_BSRATING,
                AVG(bsavgimp) v_avg_BSAVGIMP, $v_Full_Sample_AVGIMP , AVG(bsu) v_avg_bsu ,$v_valid_loop_count, $p_bootstrap_loop
                       FROM bootstrap_result where job_id=$job_id and group_id=$group_id """).repartition(1)*/
         var v_final_df=  spark.sql(
           s""" SELECT job_id, group_id ,cast(avg(bsrating) as decimal(30,5)) avg_bsrating,
                        cast(stddev(bsrating) as decimal(30,5)) std_error,
                        cast(AVG(bsavgimp) as decimal(30,5)) avg_bsavgimp, cast($v_Full_Sample_AVGIMP  as decimal(30,5) )  full_sample_avgimp,
                        cast(AVG(bsu) as decimal(30,5)) avg_bsu ,$v_valid_loop_count valid_loop_count, $p_bootstrap_loop input_loop_count
                        FROM bootstrap_result
                        where job_id=$job_id and group_id=$group_id  group by job_id, group_id""")

          v_final_df.write.mode("append").saveAsTable("bootstrap_final_result")
        }
        println(s"==================boostrap_result for $group_id=========")
        var df_bootstrap_result = spark.sql(s"select * from bootstrap_result where job_id=$job_id and group_id=$group_id")
        df_bootstrap_result.show()
        //df_bootstrap_result.write.mode("append").saveAsTable("bootstrap_result")
        println(s"==================End boostrap_result for $i=========")
        raise_Info("Bootstrap_processing", "INFO", "349", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing] ====== Loading final result in csv ======")

        raise_Info("Bootstrap_processing", "INFO", "351", s"[${LocalDateTime.now()}][$group_id][Bootstrap_processing]  ==============End of final bootstrap process ============")
        // } //end of loop


      } catch {
        case t: Throwable => t.printStackTrace(System.out)
          println(t.toString)
          v_status = "Error -1"
          error_msg =t.toString
      } //end of loop v_total_bootstrap_id
    }
    catch {
      case t: Throwable => t.printStackTrace(System.out)
        println(t.toString)
        v_status = "Error -2"
        error_msg =t.toString
    }
  } //end of loop group id
 // spark.sql(s"insert into bootstrap_job values ($job_id,'${LocalDateTime.now()}' ,'$v_status') ").repartition(1)
  v_job_df =spark.sql(s"select $job_id job_id,'$v_household_tblname' job_hh_table,'$v_load_vhr_tblname' job_vhr_table,'$p_config_filename' config_file_name,'${LocalDateTime.now()}'  job_date,'$v_status' job_status")
  v_job_df.write.mode("append").saveAsTable("bootstrap_job")

  val result=spark.sql(s"SELECT b.job_id,i.group_id,i.start_date,avg_bsrating,std_error,avg_bsavgimp,full_sample_avgimp,avg_bsu," +
    s" valid_loop_count,input_loop_count " +
    s" FROM bootstrap.bootstrap_final_result b join bootstrap.input_parameter i " +
    s" where b.job_id=$job_id and i.job_id=b.job_id and i.group_id=b.group_id order by i.group_id ")

  var result_df_head ="<html><body><table  border=1><thead><tr> <th>Job_id</th> <th>Group_id</th><th>Start_date</th> <th>Avg_bsrating</th> <th>Std_error</th> <th>Avg_bsavgimp</th> <th>Full_sample_avgimp</th> <th>Avg_bsu</th> <th>Valid_loop_count</th> <th>Input_loop_count</th> </tr><thead>"
  var result_df_st:String =""
  result.show()
  var jobid =""
  var groupid=""
  var Start_date=""
  var avg_bsrating=""
  var std_error=""
  var avg_bsavgimp=""
  var full_sample_avgimp=""
  var avg_bsu =""
  var valid_loop_count =""
  var input_loop_count =""
    result.select("*").collect().foreach { data =>
    try {  jobid = data.get(0).toString()} catch {case ex: Exception => println("")}
    try {  groupid = data.get(1).toString()} catch {case ex: Exception => println("")}
    try {  Start_date = data.get(2).toString()} catch {case ex: Exception => println("") }
    try {  avg_bsrating = data.get(3).toString()} catch {case ex: Exception => println("") }
    try {  std_error = data.get(4).toString()} catch {case ex: Exception => println("") }
    try {  avg_bsavgimp = data.get(5).toString()} catch {case ex: Exception => println("") }
    try {  full_sample_avgimp = data.get(6).toString()} catch {case ex: Exception => println("")}
    try {  avg_bsu = data.get(7).toString()} catch {case ex: Exception =>println("") }
    try {  valid_loop_count = data.get(8).toString()} catch {case ex: Exception => println("")}
    try {  input_loop_count = data.get(9).toString()} catch {case ex: Exception => println("") }

      result_df_st = result_df_st + s"<tr><td>$jobid</td><td>$groupid</td><td>$Start_date</td><td>$avg_bsrating</td><td>$std_error</td><td>$avg_bsavgimp</td><td>$full_sample_avgimp</td><td>$avg_bsu</td><td>$valid_loop_count</td><td>$input_loop_count</td>"
      // println(result_df_st)

  }
  result_df_head=result_df_head+result_df_st +"</table></body></html>"
  //println("html :"+result_df_head)
  var subject =s"Job id $job_id - Status:$v_status for Bootstrap Process "
  var content = s"Hi All,<br>" +
    s"<br>Please find the result below:<br>******************************************<br>" +
    s" Input tables:   $v_household_tblname and $v_load_vhr_tblname " +
    s" COnfig file name : $p_config_filename " +
    s" Error :$error_msg<br>" +
    s" O/p : ${result_df_head} <br> " +
    s"<br>******************************************" +
    s" <br>Thanks," +
    s" <br> Big data Team" +
    s"<br><br><br> Note: In case of any inconsistency of result kindly check the log files " +
    s"<br>*** This is an automatically system generated email. *** "

  println(content)
 // sendMail(subject,content)

  raise_Info("Bootstrap_processing", "INFO", "362", s"[${LocalDateTime.now()}][Bootstrap_processing]  ======================= End of bootstrap processing ()===================")

}