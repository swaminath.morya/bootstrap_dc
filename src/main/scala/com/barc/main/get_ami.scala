package com.barc.main

import com.barc.common.raise_Info
import com.barc.main.Bootstrap_processing.{spark, v_pre_job_id}
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

import java.time.LocalDateTime
import java.util.Date

/*
* Code: get_ami() main object
* About: This is main core of bootstrap process. Which calculate the rating based on sample
*        It is redeveloped from postgres procedure to Spark scala code
* Developer: Big data Team  (Swaminath, Monas)
* Date: 3-Apr-2022
* */
import spark.implicits._
object get_ami {
  var v_coalesce_part=6

  def generate_series = ((cell_id_size:Int,max_id:Int,min_id:Int) => {
   // var series = (0 to cell_id_size).toArray
    //collect_list(random)
  //s"select collect_set( (round(rand() * ($max_id - $min_id)) + $min_id)) random" +
   /* var series = spark.sql(s"  select collect_set(random) random_num from (select round((rand()*($max_id - $min_id)+$min_id),0)  random" +
      s" from (select posexplode(split(space($cell_id_size-1),' ')) as (i,x))seq )x ").head().get(0).toString*/
   /* var series = spark.sql(s" select  concat_ws(',',collect_list(cast(random as string)))  random_num from" +
      s" (select (round((rand() * cast(($max_id - $min_id) as double)) + cast($min_id as double),0)) AS random " +
      s"  from (select posexplode(split(space($cell_id_size-1),' ')) as (i,x))seq) a ")*/
    val df= spark.range(cell_id_size)
    val  rand_df   = df.select($"id",round((rand()*max_id-min_id)+min_id) as "random")
    val rand_new_df = rand_df.agg(collect_list("random").as("rand_new"))
    val series_df = rand_new_df.select(concat_ws(",", $"rand_new").alias("random_num")).head.get(0).toString
    series_df
  }
    )
  val generate_seriesUDF = udf(generate_series)


  def get_ami_proc(spark:SparkSession,p_age_low:Int,p_gender:Int,p_age_high:Int,
                   p_tp_begin:Int,p_tp_end:Int,p_repeat_days:Int,p_repeat_weeks:Int,
                   p_bootstrap_loop:Int,p_age_range:Array[Array[Int]],p_start_date:String,
                   p_sample_size:Int,p_session_id: Int, p_tvset_id: Int,p_sql_date_range: String,group_id :String,job_id:Int): Unit =
  {
    spark.catalog.clearCache()
    spark.udf.register("generate_seriesUDF", generate_series)
    raise_Info("get_ami_proc","INFO","49",s"[$group_id][get_ami] [${LocalDateTime.now()}] ======================= start get_ami() for jobid: $job_id===================")

    raise_Info("get_ami_proc","INFO","51",s"[$group_id][get_ami] [${LocalDateTime.now()}] start_date: $p_start_date, sample_size: ${p_sample_size.toString}, bootstrap_count: $p_bootstrap_loop '")

    raise_Info("get_ami_proc","INFO","53",s"[$group_id][get_ami] [${LocalDateTime.now()}] ------------- Start Calculating ASAvgIMP & BSRating----------------")

    var v_d = ((p_tp_end - p_tp_begin) / 60.0) * p_repeat_days *p_repeat_weeks

    var p_debug=true
    var v_seed_value =2.2
    //var v_bst_id =1
    var parallel_batch = (1 to p_bootstrap_loop).toSeq
    println("Sequence ", parallel_batch)
    parallel_batch.par.foreach { k =>
     //  for( k <- 1 to p_bootstrap_loop) {

      // val config = new SparkConf().setAppName(s"BootstrapAPI_loop_$k").set("spark.executor.extraClassPath", sys.props("java.class.path"))
      //  val spark_loop = SparkSession.builder().config(config).enableHiveSupport().getOrCreate()
      //val spark_loop =SparkSession.builder().appName(s"BootstrapAPI_loop_$k").enableHiveSupport().getOrCreate()

      // import spark_loop.implicits._
      //println("Spark Session is Initiated :"+spark_loop +"*****************")
      raise_Info("get_ami_proc","INFO","71",s"(Round) --- Start loop ${k}/$p_bootstrap_loop in bootstrap_count (unit of d is minute) -------")

      household_sample_by_primary_control(spark, p_sample_size, p_sql_date_range, p_debug, (v_seed_value+k).round.toInt,group_id :String,job_id:Int,k:Int)
      get_sample_adjustment_ratio(spark,p_age_range:Array[Array[Int]],group_id :String, job_id:Int,k:Int)
      raise_Info("get_ami_proc","INFO","75",s"[$group_id][get_ami] [${LocalDateTime.now()}] Step B): end building post_stratify_sample_$p_bootstrap_loop & post_stratify_factor tables")

      var v_sql_statement =
        s""" SELECT   b.*,c.bootstrap_loop, c.adjustment_factor, (b.weight / COALESCE(c.adjustment_factor,1.0)) AS adjusted_weight,c.group_id
            FROM member_universe b join post_stratify_factor_flat_$k c
            on( b.state = c.state
             AND b.gender=c.gender AND b.tp_date=c.tp_date
              and b.job_id=c.job_id and b.job_id=$job_id
              and c.bootstrap_loop=$k
              and c.group_id=$group_id)
              where  ($p_gender = 0 OR b.gender = $p_gender)
              AND (b.age >= $p_age_low) AND (b.age <= $p_age_high)
              AND b.age>=c.low_age and b.age<=c.high_age
              AND ($p_sql_date_range)
        """

      var  df_member_universe_adjusted_weight  =spark.emptyDataFrame
      df_member_universe_adjusted_weight=  spark.sql( v_sql_statement)
      df_member_universe_adjusted_weight.createOrReplaceTempView(s"member_universe_adjusted_weight_$k")
      /*  synchronized {
          df_member_universe_adjusted_weight.coalesce(v_coalesce_part)
            .write.option("path", s"s3://emr-rolling-avg/bootstrap/member_universe_adjusted_weight")
            .partitionBy("job_id", "group_id", "bootstrap_loop").mode("append").saveAsTable("member_universe_adjusted_weight")
        }*/
      raise_Info("get_ami_proc","INFO","99",s"[$group_id][get_ami] [${LocalDateTime.now()}] (Round) Dynamic SQL member_universe_adjusted_weight ")

      //  println(s" df_member_universe_adjusted_weight :"+ df_member_universe_adjusted_weight.count())
      //df_member_universe_adjusted_weight.count.toInt
      // df_member_universe_adjusted_weight.show(10,false)


      raise_Info("get_ami_proc","INFO","106",s"[$group_id][get_ami] [${LocalDateTime.now()}] (Round) Step D): End building member_universe_adjusted_weight_$k TABLE.")
      v_sql_statement =
        s"""SELECT distinct  a.job_id,a.group_id,b.bootstrap_loop,b.household_id, b.member_id, b.tp_date, b.age, b.gender, a.dup_count, b.weight,
                          a.dup_count * b.adjusted_weight AS dup_adjusted_weight
                          FROM household_sample_dup_count_$k a, member_universe_adjusted_weight_$k b
                                WHERE a.household_id = b.household_id AND a.tp_date = b.tp_date
                                 and a.job_id=$job_id  and a.job_id=b.job_id
                                 and a.group_id=$group_id  and a.group_id=b.group_id
                                 and b.bootstrap_loop=$k  and b.bootstrap_loop=a.bootstrap_loop  """

      raise_Info("get_ami_proc","INFO","116",s"[$group_id][get_ami] [${LocalDateTime.now()}] (Round) Dynamic SQL v_sql_statement: $v_sql_statement")
      var df_household_sample_for_bsu =spark.emptyDataFrame
      df_household_sample_for_bsu =spark.sql( v_sql_statement)
      df_household_sample_for_bsu.createOrReplaceTempView(s"household_sample_for_bsu_$k")
      // df_household_sample_for_bsu.show(false)
      //  var v_count = df_household_sample_for_bsu.count()
      /* synchronized {
         df_household_sample_for_bsu.coalesce(v_coalesce_part).write.option("path", s"s3://emr-rolling-avg/bootstrap/household_sample_for_bsu")
           .partitionBy("job_id", "group_id", "bootstrap_loop").mode("append").saveAsTable("household_sample_for_bsu")
       }*/
     /* var v_count = spark.sql(s"SELECT count(*) cnt  FROM household_sample_for_bsu_$k" +
        s" where job_id=$job_id and group_id=$group_id and bootstrap_loop=$k")*/

      raise_Info("get_ami_proc","INFO","129",
        s"[$group_id][get_ami] [${LocalDateTime.now()}] (Round) Step D): End building household_sample_for_bsu_$k TABLE. There are ${df_household_sample_for_bsu.count()} unique household-member-dates rows.")

      var v_job_str=""
      if (v_pre_job_id==0)
      {
        v_job_str=s" and job_id=$job_id"
      }
      else
      {
        v_job_str=s" and job_id=$v_pre_job_id"
      }
      var v_sql_date_range =p_sql_date_range

      var vvrf_qry=s"""
        SELECT $job_id job_id,$group_id group_id,$k bootstrap_loop,week, household_id, member_id, nccs, gender, age, tvset_id, session, cast(tp_begin as BIGINT) tp_begin,
        cast(tp_end as BIGINT) tp_end, tp_date, state, townvillagecode,weight
          FROM viewer_viewing_records_orig b
           WHERE ($p_gender = 0 OR gender = $p_gender)
                  AND  (b.age >= $p_age_low) AND (b.age <= $p_age_high)
                  AND ($p_session_id = 0 OR session = $p_session_id )
                  AND ($p_tvset_id = 0 OR tvset_id = $p_tvset_id)
                  AND tp_begin <= $p_tp_end AND tp_end >= $p_tp_begin
                  AND ($p_sql_date_range)
                  $v_job_str
                  ORDER BY tp_date, household_id, member_id, gender, age, state, nccs, townvillagecode
      """
      var viewer_viewing_records_filtered =spark.emptyDataFrame
      viewer_viewing_records_filtered =spark.sql(vvrf_qry)
    //  println(vvrf_qry)
     // println(s"Data count - for loop $k-",viewer_viewing_records_filtered.count())
      viewer_viewing_records_filtered.createOrReplaceTempView(s"viewer_viewing_records_filtered_$k")
     /*  synchronized {
          viewer_viewing_records_filtered.coalesce(v_coalesce_part).write.option("path", s"s3://emr-rolling-avg/bootstrap/viewer_viewing_records_filtered")
            .partitionBy("job_id", "group_id", "bootstrap_loop").mode("append").saveAsTable("viewer_viewing_records_filtered")
        }*/
      raise_Info("get_ami_proc", "INFO", "165", s"viewer_viewing_records_filtered\n query:$vvrf_qry ")

      //((sum(upper((b.tp_begin, b.tp_end) * ('$p_tp_begin ', '$p_tp_end')) - lower((b.tp_begin, b.tp_end) * ('$p_tp_begin', '$p_tp_end')))/ 60.0)) AS viewing_time_minute
      // AND b.tp_date = c.tp_date
      v_sql_statement =
        s"""SELECT b.job_id,b.group_id,b.bootstrap_loop,b.state, b.tp_date, b.household_id, b.member_id, b.weight, c.adjustment_factor,
          (b.weight/COALESCE(c.adjustment_factor,1.0)) AS adjusted_weight,
         sum(greatest( greatest(b.tp_begin,$p_tp_begin) ,least(b.tp_end,$p_tp_end) )  - least( greatest(b.tp_begin,$p_tp_begin) , least(b.tp_end,$p_tp_end) ))/60 AS viewing_time_minute
          FROM viewer_viewing_records_filtered_$k b join  post_stratify_factor_flat_$k  c on
          ( b.state = c.state
         and  from_unixtime(unix_timestamp(b.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd')=from_unixtime(unix_timestamp(c.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd')
          AND b.gender = c.gender
          and b.job_id= $job_id  and b.job_id =c.job_id
          and b.group_id= $group_id and b.group_id =c.group_id
          and b.bootstrap_loop=$k and   b.bootstrap_loop =c.bootstrap_loop)
          where b.age>=c.low_age and b.age<=c.high_age
          GROUP BY b.job_id,b.group_id,b.bootstrap_loop,b.state, b.tp_date, b.household_id, b.member_id, b.weight, c.adjustment_factor"""
      raise_Info("get_ami_proc","INFO","182",s"[$group_id][get_ami] [${LocalDateTime.now()}] (Round) Dynamic SQL v_sql_statement: $v_sql_statement")
      var df_member_sample_adjusted_weight =spark.emptyDataFrame
      df_member_sample_adjusted_weight= spark.sql(v_sql_statement)
      /* synchronized {
         df_member_sample_adjusted_weight.coalesce(v_coalesce_part).write.option("path",s"s3://emr-rolling-avg/bootstrap/member_sample_adjusted_weight")
           .partitionBy("job_id","group_id","bootstrap_loop").mode("append").saveAsTable("member_sample_adjusted_weight")
       }*/
      df_member_sample_adjusted_weight.createOrReplaceTempView(s"member_sample_adjusted_weight_$k")
      raise_Info("get_ami_proc","INFO","190",s"[$group_id][get_ami] [${LocalDateTime.now()}]create table member_sample_adjusted_weight_$k  " )

      // s"           AND (b.age >= '$p_age_low') AND (b.age <= '$p_age_high')  AND ('$v_sql_date_range')" +
      var df_member_sample_fs  =spark.emptyDataFrame
      v_sql_statement=  s"""SELECT distinct  b.job_id,$group_id group_id,$k bootstrap_loop,b.household_id, b.member_id, b.gender, b.age, b.tp_date, b.weight
                          FROM member_universe b
                            WHERE ($p_gender = 0 OR b.gender = $p_gender)
                                 AND (b.age >= $p_age_low) AND (b.age <= $p_age_high) and b.job_id=$job_id
                                 ORDER BY b.household_id, b.member_id, b.gender, b.age, b.tp_date """
      df_member_sample_fs  =  spark.sql(v_sql_statement)
       df_member_sample_fs.createOrReplaceTempView(s"member_sample_fs_$k")

    //  println(s"member_sample_fs_$k count:${df_member_sample_fs.count()}  :query :$v_sql_statement" )
     /* synchronized {
        df_member_sample_fs.coalesce(v_coalesce_part).write.mode("overwrite").saveAsTable(s"member_sample_fs_$k")
      }//option("path",s"s3://emr-rolling-avg/bootstrap/member_sample_fs").partitionBy("job_id","group_id","bootstrap_loop")
*/
// AND a.tp_date = b.tp_date

      //  sum(greatest( greatest(b.tp_begin,$p_tp_begin) ,least(b.tp_end,$p_tp_end) )  - least( greatest(b.tp_begin,$p_tp_begin) , least(b.tp_end,$p_tp_end) ))/60 AS viewing_time_minute

      v_sql_statement = s"""SELECT distinct a.job_id,a.group_id,a.bootstrap_loop,a.household_id, a.member_id, a.tp_date,
                            a.weight * sum( case when greatest(b.tp_begin,$p_tp_begin) < least(b.tp_end,$p_tp_end) then
                                           least(b.tp_end,$p_tp_end)- greatest(b.tp_begin,$p_tp_begin) else 0 end)/60.0 AS bsu_avg_impression,
                            sum( case when greatest(b.tp_begin,$p_tp_begin) < least(b.tp_end,$p_tp_end) then
                                           least(b.tp_end,$p_tp_end)- greatest(b.tp_begin,$p_tp_begin) else 0 end) AS duration
                              FROM member_sample_fs_$k a, viewer_viewing_records_filtered_$k b
                              WHERE a.household_id = b.household_id AND a.member_id = b.member_id
                              and from_unixtime(unix_timestamp(a.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd')=from_unixtime(unix_timestamp(b.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd')
                              and a.job_id=$job_id and b.job_id=$job_id and a.job_id=b.job_id
                              and a.group_id=$group_id and b.group_id=$group_id and a.group_id=b.group_id
                              and a.bootstrap_loop=$k and b.bootstrap_loop=$k and b.bootstrap_loop=a.bootstrap_loop
                            GROUP BY a.job_id,a.group_id,a.bootstrap_loop,a.household_id, a.member_id, a.tp_date, a.weight"""
      var df_member_sample_avg_imp_fs =spark.emptyDataFrame
      df_member_sample_avg_imp_fs=spark.sql(v_sql_statement)
      df_member_sample_avg_imp_fs.createOrReplaceTempView(s"member_sample_avg_imp_fs_$k")
      /* synchronized {
         df_member_sample_avg_imp_fs.coalesce(v_coalesce_part).write.option("path",s"s3://emr-rolling-avg/bootstrap/member_sample_avg_imp_fs")
           .partitionBy("job_id","group_id","bootstrap_loop").mode("append").saveAsTable("member_sample_avg_imp_fs")
       }*/
      raise_Info("get_ami_proc","INFO","230",s"[$group_id][get_ami] [${LocalDateTime.now()}]create table member_sample_avg_imp_fs_$k ")

      var v_FSVACCUM =0.0
      v_FSVACCUM =  spark.sql(s"SELECT nvl(SUM(bsu_avg_impression),0)  " +
        s"  FROM member_sample_avg_imp_fs_$k " +
        s" where job_id=$job_id and group_id=$group_id and bootstrap_loop=$k").head().get(0).toString.toDouble
     //  println(s"v_FSVACCUM $k - $v_FSVACCUM")
      var v_FSAVGIMP =0.0
      v_FSAVGIMP =v_FSVACCUM / v_d;
     //  println("v_FSVACCUM", v_FSVACCUM,"v_d",v_d,"v_FSAVGIMP",v_FSAVGIMP)

      var df_bootstrap_result_fs =spark.sql(s" select $job_id job_id,$group_id group_id,$k bootstrap_loop," +
        s" cast($v_FSVACCUM as decimal(30,10) ) bsvaccum,$v_d d,cast($v_FSAVGIMP as decimal(30,10)) fsavgimp")

     /* df_bootstrap_result_fs.createOrReplaceTempView(s"bootstrap_result_fs_$k")*/
     synchronized {
        df_bootstrap_result_fs.coalesce(1).write
          .mode("append").saveAsTable("bootstrap_result_fs")
      }
      raise_Info("get_ami_proc","INFO","249",s"[$group_id][get_ami] [${LocalDateTime.now()}]create table bootstrap_result_fs")

      v_sql_statement =s"""SELECT    a.JOB_ID,a.group_id,a.bootstrap_loop,b.state, b.tp_date, b.household_id, b.member_id,
                             b.adjusted_weight * a.dup_count as dup_adjusted_weight, b.viewing_time_minute
                        FROM household_sample_dup_count_$k a join member_sample_adjusted_weight_$k b
                             on ( a.state = b.state
                             AND from_unixtime(unix_timestamp(a.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd') = from_unixtime(unix_timestamp(b.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd')
                             AND a.household_id = b.household_id
                             and a.job_id=$job_id and b.job_id=$job_id and a.job_id=b.job_id
                             and a.group_id=$group_id and b.group_id=$group_id and a.group_id=b.group_id
                             and a.bootstrap_loop=$k and b.bootstrap_loop=$k and b.bootstrap_loop=a.bootstrap_loop)
                        ORDER By a.job_id,a.group_id,a.bootstrap_loop,b.state, b.tp_date, b.household_id, b.member_id"""

      raise_Info("get_ami_proc","INFO","262",s"[$group_id][get_ami] [${LocalDateTime.now()}] (Round) Dynamic SQL member_sample_avg_imp_$k: \n $v_sql_statement")

      var df_member_sample_avg_imp =spark.emptyDataFrame
      df_member_sample_avg_imp= spark.sql(v_sql_statement)
      var v_count_member_sample = 0.0
      v_count_member_sample = df_member_sample_avg_imp.count()
      df_member_sample_avg_imp.createOrReplaceTempView(s"member_sample_avg_imp_$k")
      /* synchronized {
           df_member_sample_avg_imp.coalesce(v_coalesce_part).write.option("path",s"s3://emr-rolling-avg/bootstrap/member_sample_avg_imp")
           .partitionBy("job_id","group_id","bootstrap_loop").mode("append").saveAsTable("member_sample_avg_imp")
       }*/

      raise_Info("get_ami_proc","INFO","274",s"[$group_id][get_ami] [${LocalDateTime.now()}] (Round) Step D): End building member_sample_avg_imp_$k TABLE. There are ${v_count_member_sample} unique state-household-member-dates rows.")
     // println(s" (Round) Step D): End building member_sample_avg_imp_$k TABLE. There are ${v_count_member_sample} unique state-household-member-dates rows.")
      var v_BSU =0.0
      v_BSU =spark.sql( s"""SELECT nvl(cast(avg(daily_weight_sum) as decimal(20,5)),0) avg_dws FROM
      (SELECT nvl(sum(dup_adjusted_weight),0) daily_weight_sum FROM household_sample_for_bsu_$k
      where job_id=$job_id and group_id=$group_id and bootstrap_loop=$k
      GROUP BY job_id,group_id,bootstrap_loop,tp_date) a """).head().get(0).toString().toDouble
     // var v_BSU =0.0
      // v_BSU =(v_BSU_df.select("avg_dws").head().get(0)).toString().toDouble
      raise_Info("get_ami_proc","INFO","283",s"[$group_id][get_ami] [${LocalDateTime.now()}] Step C): Calculate total weight (BSU=$v_BSU) of the bootstrap sample")
      //var v_BSVACCUM_df =spark.emptyDataFrame
      var v_BSVACCUM =0.0
       v_BSVACCUM =spark.sql(s""" SELECT nvl(cast(SUM(dup_adjusted_weight * viewing_time_minute) as decimal(20,5)),0) sum_daw
           FROM member_sample_avg_imp_$k where job_id=$job_id and group_id=$group_id and bootstrap_loop=$k """).head().get(0).toString.toDouble
      // v_BSVACCUM_df.show()
      //  v_BSVACCUM =(v_BSVACCUM_df.select("sum_daw").head().get(0)).toString.toDouble

      raise_Info("get_ami_proc","INFO","291",s"[$group_id][get_ami] [${LocalDateTime.now()}] Fetch v_BSU:$v_BSU & v_BSVACCUM:$v_BSVACCUM")
      var v_BSAVGIMP = 0.0
      v_BSAVGIMP = (v_BSVACCUM / v_d)
      if (v_BSAVGIMP.isNaN ==true) { v_BSAVGIMP =0.0 }

      var v_BSRATING = 0.0
      v_BSRATING =(v_BSAVGIMP / v_BSU)
     // print("v_BSRATING",v_BSRATING)
      if (v_BSRATING.isNaN ==true) { v_BSRATING =0.0 }
      raise_Info("get_ami_proc","INFO","300",s"[$group_id][get_ami] [${LocalDateTime.now()}] Fetch v_BSAVGIMP:$v_BSAVGIMP & v_BSRATING:$v_BSRATING")

      var v_Full_Sample_AVGIMP_df = spark.sql(
        s"""SELECT nvl(cast(fsavgimp as decimal(30,5)),0)
           |FROM bootstrap_result_fs where job_id=$job_id and group_id=$group_id and bootstrap_loop=$k """.stripMargin)
      var v_Full_Sample_AVGIMP =v_Full_Sample_AVGIMP_df.head().get(0).toString().toDouble

     // println(s"bootstrap_result :($v_BSVACCUM bsvaccum, $v_d d, $v_BSAVGIMP bsavgimp, $v_BSU bsu, $v_BSRATING bsrating, $v_Full_Sample_AVGIMP fsavgimp) ")

      var bootstrap_result_df=spark.emptyDataFrame
      bootstrap_result_df=   spark.sql(
        s"""select  $job_id job_id,$group_id group_id,$k id , cast($v_BSVACCUM as decimal(30,10)) bsvaccum,
           | cast($v_d as decimal(30,10))  d, cast($v_BSAVGIMP  as decimal(30,10) ) bsavgimp, cast($v_BSU as decimal(30,10)) bsu, cast($v_BSRATING  as decimal(30,10)) bsrating,
           | cast($v_Full_Sample_AVGIMP as decimal(30,10)) fsavgimp,current_timestamp() ts  from (select 1 ) tmp  """.stripMargin)

      // bootstrap_result_df.show()
    //  println("Saving bootstrap_result dataframe .... ")
      try{
        synchronized {
          bootstrap_result_df.write
            .mode("append").saveAsTable("bootstrap_result")
        }
      } catch {
        case ex: Exception =>
          println("Got error while saving bootstrap_result :"+ex)
          println("====================================================================")
          sys.exit(0)
      }

      println("Saving bootstrap_result dataframe completed .... ")
      // v_bst_id = spark.sql(s"select nvl(max(id),0)+1 from bootstrap_result where job_id=$job_id and group_id=$group_id").head().get(0).toString.toInt
      raise_Info("get_ami_proc","INFO","331",s"[$group_id][get_ami] [${LocalDateTime.now()}] (Round) --- End loop ${k} in bootstrap_count (unit of d is minute) -------")
      raise_Info("get_ami_proc","INFO","332",s"[$group_id][get_ami] [${LocalDateTime.now()}] (Round) BSAVGIMP($v_BSAVGIMP) = BSVACCUM($v_BSVACCUM) / d($v_d)'")
      raise_Info("get_ami_proc","INFO","333",s"[$group_id][get_ami] [${LocalDateTime.now()}] (Round) BSRATING($v_BSRATING) = BSAVGIMP($v_BSAVGIMP) / BSU($v_BSU)")
      raise_Info("get_ami_proc","INFO","334",s"[$group_id][get_ami] [${LocalDateTime.now()}] (Round) -------------------------------------------------------------------")
      //spark_loop.close()

    }//parallel loop



    raise_Info("get_ami_proc","INFO","341",s"[$group_id][get_ami] [${LocalDateTime.now()}] ======================= End of get_ami()===================")

  }



  def household_sample_by_primary_control(spark:SparkSession,p_sample_size:Int,p_sql_date_range:String,
                                          p_debug:Boolean, v_seed_value:Int, group_id :String,job_id:Int,p_bootstrap_loop:Int): Unit ={

    raise_Info("get_ami_proc","INFO","350",s"[$group_id][${LocalDateTime.now()}]household_sample_by_primary_control]  ====== start household_sample_by_primary_control() ==========")

    raise_Info("get_ami_proc","INFO","352",s"[$group_id][${LocalDateTime.now()}][household_sample_by_primary_control] The current value for sample_size is $p_sample_size")


    var v_statement =""
    if (p_sample_size < 1)
    {
      v_statement =s""" SELECT job_id, group_id,$p_bootstrap_loop bootstrap_loop,tp_date, COUNT(DISTINCT(household_id, state, nccs, townvillagecode)) AS sample_size
                        FROM household_universe where job_id=$job_id and group_id=$group_id GROUP BY job_id,group_id,tp_date ORDER BY tp_date"""
    }
    else {
      v_statement=s""" SELECT job_id,tp_date,$p_bootstrap_loop bootstrap_loop, $p_sample_size as sample_size
                            FROM household_universe where job_id=$job_id and group_id=$group_id GROUP BY job_id,tp_date ORDER BY tp_date"""
    }
    raise_Info("get_ami_proc","INFO","365",s"household_sample_size_$p_bootstrap_loop \n $v_statement")
    var household_sample_size =spark.sql(v_statement)
    // raise_Info("get_ami_proc","INFO","207",s"household_sample_size_$p_bootstrap_loop:${household_sample_size.count} ")
    raise_Info("get_ami_proc","INFO","368",s"[${LocalDateTime.now()}] household_sample_size_$p_bootstrap_loop ")

    /* synchronized {
       household_sample_size.coalesce(v_coalesce_part).write.option("path",s"s3://emr-rolling-avg/bootstrap/household_sample_size")
         .partitionBy("job_id","group_id","bootstrap_loop").mode("append").saveAsTable("household_sample_size")
     }*/
    // spark.catalog.dropTempView(s"household_sample_size_$p_bootstrap_loop")
    household_sample_size.createOrReplaceTempView(s"household_sample_size_$p_bootstrap_loop")
    //a.tp_date = b.tp_date  AND a.tp_date = c.tp_date
    v_statement =
      s"""
        |SELECT * from
        |		(
        |			SELECT a.job_id,a.group_id, c.bootstrap_loop,a.tp_date, a.state, a.nccs, a.townvillagecode, FIRST_VALUE(a.id) OVER w AS min_id, LAST_VALUE(a.id) OVER w AS max_id,
        |			(b.household_percent_dist * c.sample_size) AS cell_sample_size, count(a.id) OVER w as cell_id_size
        |			FROM household_universe a, universe_primary_control b, household_sample_size_$p_bootstrap_loop c
        |			WHERE a.tp_date = b.tp_date AND a.state = b.state AND a.nccs = b.nccs AND a.townvillagecode = b.townvillagecode AND a.tp_date = c.tp_date
        |			and a.job_id=b.job_id and a.job_id=$job_id and a.group_id=b.group_id and a.group_id=$group_id and c.bootstrap_loop=$p_bootstrap_loop
        |			GROUP BY a.job_id,a.group_id, c.bootstrap_loop, a.id, a.tp_date, a.state, a.nccs, a.townvillagecode, b.household_percent_dist, c.sample_size
        |			WINDOW w AS (PARTITION BY a.tp_date, a.state, a.nccs, a.townvillagecode ORDER BY a.id ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
        |		)d
        |		GROUP BY job_id,group_id,bootstrap_loop,cell_id_size,tp_date,state,nccs,townvillagecode,min_id,max_id,cell_sample_size ORDER BY tp_date, cell_id_size
        |""".stripMargin
    // println(v_statement)
    var df_random_id_mapping_tmp = spark.sql(v_statement)
    df_random_id_mapping_tmp.createOrReplaceTempView(s"random_id_mapping_tmp_$p_bootstrap_loop")
     //  df_random_id_mapping_tmp.coalesce(v_coalesce_part).write.mode("overwrite").saveAsTable(s"random_id_mapping_tmp_$p_bootstrap_loop")
    raise_Info("get_ami_proc","INFO","395",s"[$group_id][${LocalDateTime.now()}] random_id_mapping_tmp_$p_bootstrap_loop created ")

try {

 // bootdf.withColumn("range", sequence(lit(0), col("sc"))).show(false)
  var df_random_id_mapping_explode_tmp = df_random_id_mapping_tmp.
    withColumn("random_num",sequence(lit(1), col("cell_id_size")))
//  println("COUNT :-----"+df_random_id_mapping_explode_tmp.count())
  //df_random_id_mapping_explode_tmp.printSchema()
   // df_random_id_mapping_explode_tmp.write.mode("overwrite").saveAsTable(s"random_id_mapping_tmp_explode_$p_bootstrap_loop")
  df_random_id_mapping_explode_tmp.createOrReplaceTempView(s"random_id_mapping_tmp_explode_$p_bootstrap_loop")
}
    catch {
      case t: Throwable => t.printStackTrace(System.out)
        println(t.toString)

  }
    //random() * (max - min) + min;
    raise_Info("get_ami_proc","INFO","413",s"[$group_id][${LocalDateTime.now()}]household_sample_by_primary_control] Step 1: Create random_id_mapping_tmp_$p_bootstrap_loop (to get min_id, max_id in each PARTITION)")
    var query=s"""
      SELECT job_id,group_id,bootstrap_loop,tp_date, state ,nccs,townvillagecode,
         round((rand($v_seed_value) * cast((max_id - min_id) as double)+ cast(min_id as double)),0) AS replacement_id ,
       cell_sample_size AS cell_sample_size
      FROM random_id_mapping_tmp_explode_$p_bootstrap_loop
      LATERAL VIEW explode(random_num) random_num AS seq_num
      where job_id=$job_id and group_id=$group_id and bootstrap_loop=$p_bootstrap_loop
      order by tp_date,state,nccs,townvillagecode,cell_sample_size"""

    // floor(round(rand($v_seed_value) *(max_id-min_id)+min_id)) AS replacement_id,
   // println(query)
    var df_random_id_mapping = spark.sql(query)
    // df_random_id_mapping.show()
    df_random_id_mapping.createOrReplaceTempView(s"random_id_mapping_$p_bootstrap_loop")
    // synchronized {
   // df_random_id_mapping.coalesce(v_coalesce_part).write.mode("overwrite").saveAsTable(s"random_id_mapping_$p_bootstrap_loop")
   //   }

  //  raise_Info("get_ami_proc","INFO","228",s"random_id_mapping_$p_bootstrap_loop: ${df_random_id_mapping.count()}")
    raise_Info("get_ami_proc","INFO","432",s"[$group_id][${LocalDateTime.now()}]household_sample_by_primary_control] STEP 3: Create random_id_mapping table")

    var household_sample =spark.sql(s"""
      SELECT a.job_id,a.group_id,b.bootstrap_loop, b.tp_date,b.replacement_id, a.household_id AS replacement_household_id,
       a.state,a.nccs,a.townvillagecode, b.cell_sample_size
    FROM household_universe a join random_id_mapping_$p_bootstrap_loop  b on
      ( a.id =b.replacement_id and a.state = b.state AND a.nccs = b.nccs
        AND a.townvillagecode = b.townvillagecode)
        where
        a.job_id=b.job_id and a.job_id=$job_id
        and a.group_id=b.group_id and a.group_id=$group_id
        and b.bootstrap_loop=$p_bootstrap_loop
        AND ($p_sql_date_range)
        order by a.tp_date,a.replacement_household_id,a.state,a.nccs,a.townvillagecode """)

    raise_Info("get_ami_proc","INFO","448",s"[$group_id][${LocalDateTime.now()}]household_sample_by_primary_control]  Dynamic SQL household_sample create tbl")
    // spark.catalog.dropTempView(s"household_sample_$p_bootstrap_loop")
    household_sample.createOrReplaceTempView(s"household_sample_$p_bootstrap_loop")
    /* synchronized {
       household_sample.coalesce(v_coalesce_part).write.option("path",s"s3://emr-rolling-avg/bootstrap/household_sample")
         .partitionBy("job_id","group_id","bootstrap_loop").mode("append").saveAsTable("household_sample")
     }*/
    //raise_Info("get_ami_proc","INFO","241",s"household_sample records:${household_sample.count()}")

    raise_Info("get_ami_proc","INFO","457",s"[$group_id][household_sample_by_primary_control] STEP 4: Create household_sample by unnest random_id_array and join with household_universe table")

    val df_household_sample_dup_count = spark.sql(s"""
      SELECT job_id,group_id,bootstrap_loop,state, tp_date, replacement_household_id AS household_id, count(replacement_household_id) AS dup_count
         FROM household_sample_$p_bootstrap_loop
      where job_id=$job_id and group_id=$group_id  and bootstrap_loop=$p_bootstrap_loop
       GROUP BY job_id,group_id,bootstrap_loop,state, tp_date,replacement_household_id ORDER BY state,tp_date,replacement_household_id""")
    /*  synchronized {
        df_household_sample_dup_count.coalesce(v_coalesce_part).write.option("path",s"s3://emr-rolling-avg/bootstrap/household_sample_dup_count")
          .partitionBy("job_id","group_id","bootstrap_loop").mode("append").saveAsTable("household_sample_dup_count")
      }*/
    //  spark.catalog.dropTempView(s"household_sample_dup_count_$p_bootstrap_loop")
    df_household_sample_dup_count.createOrReplaceTempView(s"household_sample_dup_count_$p_bootstrap_loop")
    // raise_Info("get_ami_proc","INFO","251",s"household_sample: ${df_household_sample_dup_count.count}")
    raise_Info("get_ami_proc","INFO","471",s"[$group_id][${LocalDateTime.now()}]household_sample_by_primary_control] Step 5: create household_sample_dup_count_$p_bootstrap_loop")
    raise_Info("get_ami_proc","INFO","472",s"[$group_id][${LocalDateTime.now()}]household_sample_by_primary_control]  ====== end household_sample_by_primary_control() ==========")
  }

  def get_sample_adjustment_ratio(spark:SparkSession,p_age_range:Array[Array[Int]],group_id :String, job_id: Int,p_bootstrap_loop:Int): Unit ={

    raise_Info("get_ami_proc","INFO","477",
      s"[$group_id][get_sample_adjustment_ratio]  ====== start get_sample_adjustment_ratio() ======")

    // spark.sql("DROP TABLE IF EXISTS member_sample_dup_count")
    var df_member_sample_dup_count = spark.sql(s"""
                          SELECT m.*, h.group_id, h.dup_count,h.bootstrap_loop
                          from member_universe m join household_sample_dup_count_$p_bootstrap_loop h
                                on( m.state = h.state AND m.tp_date = h.tp_date AND m.household_id = h.household_id
                                and m.job_id=h.job_id and m.job_id=$job_id and  h.group_id=$group_id
                                and h.bootstrap_loop =$p_bootstrap_loop)
                        ORDER BY m.household_id, m.member_id, m.tp_date""")
    df_member_sample_dup_count.createOrReplaceTempView(s"member_sample_dup_count_$p_bootstrap_loop")
    /* synchronized {
       df_member_sample_dup_count.coalesce(v_coalesce_part).write.option("path",s"s3://emr-rolling-avg/bootstrap/member_sample_dup_count")
         .partitionBy("job_id","group_id","bootstrap_loop").mode("append").saveAsTable("member_sample_dup_count")
     }*/
    // raise_Info("get_ami_proc","INFO","271",s"member_sample_dup_count_$p_bootstrap_loop: ${df_member_sample_dup_count.count} ")


    var df_member_sample_dup_count_tmp = spark.sql(s"""  select job_id,group_id,bootstrap_loop,state, tp_date, age, gender, sum(dup_count * weight) as member_dup_weight
                                                      from member_sample_dup_count_$p_bootstrap_loop
                                                     where job_id=$job_id and group_id=$group_id and bootstrap_loop=$p_bootstrap_loop
      group by job_id,group_id,bootstrap_loop,state, tp_date, gender, age order by tp_date, gender, age""")
    df_member_sample_dup_count_tmp.createOrReplaceTempView(s"member_sample_dup_count_tmp_$p_bootstrap_loop")
    /*synchronized {
      df_member_sample_dup_count_tmp.coalesce(v_coalesce_part).write.option("path",s"s3://emr-rolling-avg/bootstrap/member_sample_dup_count_tmp")
        .partitionBy("job_id","bootstrap_loop").mode("append").saveAsTable("member_sample_dup_count_tmp")
    }*/

    //   raise_Info("get_ami_proc","INFO","279",s"member_sample_dup_count_tmp_$p_bootstrap_loop:${df_member_sample_dup_count_tmp.count} ")

    raise_Info("get_ami_proc","INFO","508",
      s"[$group_id][get_sample_adjustment_ratio]  creating post_stratify_sample_$p_bootstrap_loop table'")
    // spark.sql(s"""DROP TABLE IF EXISTS post_stratify_sample_test""")
    //  spark.sql(s"""DROP TABLE IF EXISTS post_stratify_sample""")


    raise_Info("get_ami_proc","INFO","514",s"[$group_id][${LocalDateTime.now()}][get_sample_adjustment_ratio]  created post_stratify_sample_$p_bootstrap_loop table'")
    var n = (p_age_range(0).length)
    raise_Info("get_ami_proc","INFO","516",s"[$group_id][${LocalDateTime.now()}][get_sample_adjustment_ratio]  age_range segments: $n  ")
    var i = 0

    // var df_post_stratify_sample =spark.sql("select * from post_stratify_sample_test")
    var df_post_stratify_sample_tmp =spark.emptyDataFrame

    for(  i <-0 to n-1)
    {
      var df_post_stratify_sample_final=spark.emptyDataFrame
      var v_statement=s"""
     (select a.job_id,a.group_id,a.bootstrap_loop,a.state, a.tp_date, a.age_range, a.gender, sum(a.sum) weight, aa.sum total_weight, (sum(a.sum) / aa.sum) as age_gender_ratio
				 from (select job_id, group_id,bootstrap_loop,state, tp_date, concat('${p_age_range(0)(i)}','-','${p_age_range(1)(i)}') as age_range, gender, sum(member_dup_weight) sum
						from member_sample_dup_count_tmp_$p_bootstrap_loop
						where age >= ${p_age_range(0)(i)} and age <= ${p_age_range(1)(i)}
              and  job_id= $job_id  and group_id=$group_id and bootstrap_loop=$p_bootstrap_loop
						group by job_id,group_id,bootstrap_loop,state, tp_date, gender order by tp_date, gender) AS a,
					(select job_id,group_id,bootstrap_loop,state, tp_date, sum(member_dup_weight) sum from member_sample_dup_count_tmp_$p_bootstrap_loop
					where job_id= $job_id  and group_id=$group_id and bootstrap_loop=$p_bootstrap_loop
					group by job_id,group_id,bootstrap_loop,state, tp_date order by tp_date) AS aa
				where a.state = aa.state AND a.tp_date = aa.tp_date and a.job_id=aa.job_id and a.job_id= $job_id
				and  a.group_id=aa.group_id and a.group_id= $group_id and a.bootstrap_loop=$p_bootstrap_loop
				group by a.job_id,a.group_id,a.bootstrap_loop,a.state, a.tp_date,a.age_range,a.gender, aa.sum
			) """
      // println(v_statement)
      raise_Info("get_ami_proc","INFO","540",s"post_stratify_sample -loop $p_bootstrap_loop ")
      if (i==0)
      {
        df_post_stratify_sample_tmp = spark.sql(v_statement)
      }
      else
      {
        df_post_stratify_sample_final = spark.sql(v_statement)
        df_post_stratify_sample_tmp =df_post_stratify_sample_tmp.union(df_post_stratify_sample_final)
      }

      //  df_post_stratify_sample =df_post_stratify_sample.union(df_post_stratify_sample_tmp)
      //df_post_stratify_sample.count()
      //  raise_Info("get_ami_proc","INFO","313",s"post_stratify_sample:${df_post_stratify_sample_tmp.count} ")

      /* synchronized {
         df_post_stratify_sample_tmp.coalesce(v_coalesce_part).write
           .partitionBy("job_id","group_id","bootstrap_loop").mode("append").saveAsTable("post_stratify_sample")
       }*/

    }

    df_post_stratify_sample_tmp.createOrReplaceTempView(s"post_stratify_sample_$p_bootstrap_loop")



    //var df= spark.sql("select * from post_stratify_sample")
    raise_Info("get_ami_proc","INFO","567",s"[${LocalDateTime.now()}]---------------------------------- post_stratify_sample_$p_bootstrap_loop ------------------------------------")

    var post_stratify_factor = spark.sql(s"""
                        select a.job_id,a.group_id,b.bootstrap_loop,a.state, b.tp_date, a.age_range, a.gender,a.age_gender_ratio AS universe_ratio,
                        b.age_gender_ratio AS sample_ratio, (b.age_gender_ratio/a.age_gender_ratio) AS adjustment_factor
                      from post_stratify_universe a left join post_stratify_sample_$p_bootstrap_loop b
                      on a.state=b.state AND a.age_range=b.age_range and a.gender=b.gender and a.job_id=b.job_id  and a.group_id=b.group_id
                      where b.tp_date is not null and a.job_id=$job_id and a.group_id=$group_id and b.bootstrap_loop=$p_bootstrap_loop
                      order by b.tp_date, a.age_range, a.gender
                      """)
    // raise_Info("post_stratify_factor","INFO","323",s"post_stratify_factor_$p_bootstrap_loop:${post_stratify_factor.count}  ")
    raise_Info("post_stratify_factor","INFO","578",s"post_stratify_factor_$p_bootstrap_loop ")
    post_stratify_factor.createOrReplaceTempView(s"post_stratify_factor_$p_bootstrap_loop")
    /* synchronized {
       post_stratify_factor.coalesce(v_coalesce_part).write.option("path",s"s3://emr-rolling-avg/bootstrap/post_stratify_factor")
         .partitionBy("job_id","group_id","bootstrap_loop").mode("append").saveAsTable("post_stratify_factor")
     }*/

    //order by b.tp_date, SPLIT_PART(a.age_range,'-', 1), a.gender
    raise_Info("get_sample_adjustment_ratio","INFO","586",s"[${LocalDateTime.now()}]------------------------------- post_stratify_sample_$p_bootstrap_loop -----------------------------------------------------")

    // df_post_stratify_factor_flat.printSchema()
    var df_post_stratify_factor_flat =spark.emptyDataFrame
    for( x <- 0 to p_age_range(0).length-1)
    {
      println(s"post_stratify_factor_flat loop : $x  Age range: ${p_age_range(0)(x)}-${p_age_range(1)(x)}")
      var df_post_stratify_factor_flat_tmp =spark.emptyDataFrame
      if (x==0)
      {
        df_post_stratify_factor_flat =
          spark.sql(s"SELECT job_id,group_id,bootstrap_loop,state, tp_date, ${p_age_range(0)(x)} AS low_age,${p_age_range(1)(x)} as high_age," +
            s" gender, adjustment_factor FROM post_stratify_factor_$p_bootstrap_loop " +
            s"WHERE   age_range =  '${p_age_range(0)(x)}-${p_age_range(1)(x)}'  and job_id=$job_id " +
            s" and group_id=$group_id and bootstrap_loop=$p_bootstrap_loop")

      }
      else {
        df_post_stratify_factor_flat_tmp =
          spark.sql(s"SELECT job_id,group_id,bootstrap_loop,state, tp_date, ${p_age_range(0)(x)} AS low_age,${p_age_range(1)(x)} as high_age," +
            s" gender, adjustment_factor FROM post_stratify_factor_$p_bootstrap_loop " +
            s"WHERE   age_range =  '${p_age_range(0)(x)}-${p_age_range(1)(x)}'  and job_id=$job_id " +
            s" and group_id=$group_id and bootstrap_loop=$p_bootstrap_loop")
        df_post_stratify_factor_flat =df_post_stratify_factor_flat.union(df_post_stratify_factor_flat_tmp)
      }

      /* synchronized {
         df_post_stratify_factor_flat_tmp.coalesce(v_coalesce_part).write
           .partitionBy("job_id","group_id","bootstrap_loop").mode("append").saveAsTable("post_stratify_factor_flat")
       }*/
    }
    df_post_stratify_factor_flat.createOrReplaceTempView(s"post_stratify_factor_flat_$p_bootstrap_loop")
    //df_post_stratify_factor_flat.show(10,false)

    raise_Info("get_ami_proc","INFO","620",s" [$group_id][${LocalDateTime.now()}][get_sample_adjustment_ratio]  ====== end get_sample_adjustment_ratio() ======  ")

  }

  def build_post_stratify_universe(spark:SparkSession ,p_age_range:Array[Array[Int]], group_id :String,job_id:Int)
  {
    raise_Info("get_ami_proc","INFO","626",s"[$group_id][${LocalDateTime.now()}][build_post_stratify_universe]  ======================= loading member_universe_distinct()===================")
    var member_universe_distinct =spark.emptyDataFrame
    member_universe_distinct =  spark.sql(s"""
                                              SELECT job_id,$group_id group_id,state, gender, age, SUM(weight) as aggregated_weight
                                              FROM member_universe where job_id=$job_id
                                              GROUP BY job_id,state, gender, age order by state
                                          """.stripMargin)
    synchronized {
      member_universe_distinct.coalesce(v_coalesce_part).write
        .partitionBy("job_id","group_id","state").mode("append").saveAsTable("member_universe_distinct")
    }

    // raise_Info("get_ami_proc","INFO","366",s"member_universe_distinct:${member_universe_distinct.count} ")
    raise_Info("get_ami_proc","INFO","639",s"member_universe_distinct ")
    var df_muws =spark.emptyDataFrame
    df_muws =spark.sql(
      s""" (SELECT job_id,group_id, state, sum(aggregated_weight) total_weight_by_state
         |FROM member_universe_distinct where job_id=$job_id and group_id=$group_id
         |GROUP BY job_id,group_id,state)""".stripMargin)
    synchronized {
      df_muws.coalesce(v_coalesce_part).write
        .partitionBy("job_id","group_id","state").mode("append").saveAsTable("member_universe_weight_by_state")
    }
    var  n = p_age_range(0).length

    // spark.sql("Drop table if exists post_stratify_universe ")
    for( i <- 0 to n-1) {
      var df_post_stratify_universe =spark.emptyDataFrame
      df_post_stratify_universe = spark.sql(
        s""" ( SELECT a.job_id,a.group_id,cast(a.state as Int) state, concat(${p_age_range(0)(i)} , '-' ,${p_age_range(1)(i)}) AS age_range, a.gender,
           |  sum(a.aggregated_weight) AS weight, b.total_weight_by_state AS total_weight,
           |  sum(a.aggregated_weight)/b.total_weight_by_state as age_gender_ratio
           |   FROM member_universe_distinct a, member_universe_weight_by_state b
           |   WHERE a.state=b.state
           |   AND age >= ${p_age_range(0)(i)} AND age <= ${p_age_range(1)(i)}
           |   and a.job_id=$job_id and a.job_id=b.job_id
           |   and a.group_id=$group_id and a.group_id=b.group_id
           |   GROUP BY a.job_id,a.group_id,a.state, a.gender,b.total_weight_by_state ORDER BY a.state, age_range, a.gender )""".stripMargin)

      // raise_Info("get_ami_proc","INFO","383",s"post_stratify_universe_tbl:${df_post_stratify_universe.count }")
      raise_Info("get_ami_proc","INFO","666",s"post_stratify_universe_tbl")

      synchronized {
        df_post_stratify_universe.coalesce(v_coalesce_part).write
          .partitionBy("job_id","group_id","state").mode("append").saveAsTable("post_stratify_universe")
      }

      raise_Info("get_ami","INFO","673",s"[$group_id][${LocalDateTime.now()}][build_post_stratify_universe]  ########  ====== end build_post_stratify_universe() for age >= ${p_age_range(0)(i)} AND age <= ${p_age_range(1)(i)}==========")

    }

  }

  def build_universe_primary_control(spark:SparkSession,group_id :String,job_id :Int): Unit ={

    raise_Info("get_ami","INFO","681",s"[$group_id][${LocalDateTime.now()}][build_universe_primary_control]  ====== start build_universe_primary_control() ==========")

    //removed INSERT INTO  and created view.
    var v_Statement=s"""  SELECT  job_id,$group_id group_id,row_number() over(order by tp_date,state, nccs, townvillagecode, household_id) as id,tp_date, state, nccs, townvillagecode, household_id,0 replacement_id, 0 replacement_household_id,
                      sum(weight) AS household_weight
                      FROM member_universe where job_id=$job_id
                      GROUP BY job_id,group_id,tp_date, state, nccs, townvillagecode, household_id
                      ORDER by job_id,tp_date, state, nccs, townvillagecode, household_id"""
    var df =spark.emptyDataFrame
    df =spark.sql(v_Statement)
    synchronized {
      df.coalesce(v_coalesce_part).write
        .partitionBy("job_id","group_id","state").mode("append").saveAsTable("household_universe")
    }
    // df.coalesce(v_coalesce_part).write.option("header", "true").mode("Overwrite").csv(s"C:/devops-master/Bootstrap_Dev/household_universe/")

    //raise_Info("get_ami_proc","INFO","422",s"[build_universe_primary_control]household_universe:${df.count} \n ")
    raise_Info("get_ami_proc","INFO","698",s"[build_universe_primary_control]household_universe \n ")

    v_Statement =
      s""" SELECT job_id,group_id,tp_date, count(*) AS total_household_count FROM household_universe where job_id=$job_id and group_id=$group_id GROUP BY job_id,group_id,tp_date ORDER BY tp_date""".stripMargin
    var df_hus =spark.emptyDataFrame
    df_hus= spark.sql(v_Statement)
    synchronized {
      df_hus.coalesce(v_coalesce_part).write
        .partitionBy("job_id","group_id").mode("append").saveAsTable("household_universe_size")
    }
// a.tp_date = b.tp_date
    v_Statement =s"""SELECT a.job_id,a.group_id,a.tp_date, a.state, a.nccs, a.townvillagecode, COUNT(a.household_id) household_count, b.total_household_count,
                        (COUNT(a.household_id)/b.total_household_count) AS household_percent_dist
                    FROM household_universe a, household_universe_size b
                         WHERE from_unixtime(unix_timestamp(a.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd')
                                  =from_unixtime(unix_timestamp(b.tp_date, 'yyyy-MM-dd'), 'yyyy-MM-dd')
                          and a.job_id=$job_id and a.job_id=b.job_id
                           and a.group_id=$group_id and a.group_id=b.group_id
                        GROUP BY a.job_id,a.group_id,a.tp_date, a.state, a.nccs, a.townvillagecode, b.total_household_count
                        ORDER BY a.job_id,a.group_id,a.tp_date, a.state, a.nccs, a.townvillagecode """
    var df_upc =spark.emptyDataFrame
    df_upc= spark.sql(v_Statement)
   // println(s"universe_primary_control $group_id: df_upc.count()")
    synchronized {
      df_upc.coalesce(v_coalesce_part).write
        .partitionBy("job_id","group_id","state").mode("append").saveAsTable("universe_primary_control")
    }
   // println(df_upc.count())
    raise_Info("get_ami","INFO","726",s"[$group_id][${LocalDateTime.now()}][build_universe_primary_control]  ====== End of process build_universe_primary_control() ==========")
  }

}

