package com.barc.main

import com.barc.common.Mailer.sendMail
import com.barc.common.raise_Info
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{col, lit, row_number, substring, to_date}

import java.io.File
import scala.collection.JavaConversions._
import java.text.SimpleDateFormat
import java.time.{LocalDate, LocalDateTime, Period}
import java.time.format.DateTimeFormatter
import java.time.temporal.IsoFields

/*
* Code: get_ami() main object
* About: This is Code converts the raw file to input file of HH_MEMBER & VHR
*        It is redeveloped from python & R code to Spark scala code
* Developer: Big data Team  (Saurabh)
* Date: 20-Apr-2022
* */
object hh_vhr_generator extends App {
  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  Logger.getLogger("info").setLevel(Level.OFF)
  val spark = SparkSession.builder().appName("BootstrapAPI_file_generator").enableHiveSupport().getOrCreate()
  spark.sql("create database if not exists bootstrap")
  spark.sql("use bootstrap")
  spark.sql("SET hive.exec.dynamic.partition = true")
  spark.sql("SET hive.exec.dynamic.partition.mode = nonstrict ")
  spark.sql("set hive.exec.parallel=true")
  spark.sql("set hive.exec.parallel.thread.number=8")
  spark.sql("set hive.vectorized.execution = true")
  spark.sql("set hive.vectorized.execution.enabled = true")
  //spark.sql("spark.sql.autoBroadcastJoinThreshold =-1")

  spark.conf.set("spark.sql.autoBroadcastJoinThreshold", -1)

  // val input_dir = s"C:/Users/Saurabh.kabadi/BARC Bootstrap VHR and HH"
  // val output_dir = s"C:/Users/Saurabh.kabadi/BARC Bootstrap VHR and HH/output"

  spark.sql("use bootstrap")
  var job_start = LocalDateTime.now()
  if (args.length < 4) {
    raise_Info("Bootstrap_processing", "INFO", "31", s"[%][hh_vhr_generator] === Kindly pass 2 arguments:" +
      s" \n **************************** " +
      s" \n v_path : /data/R1_Data/" +
      s" \n v_year : 2020" +
      s" \n v_start_week_no : 1-52" +
      s" \n v_end_week_no : 1-52" +
      s" \n rerun_job : A- complete run / H -only HH member file  load /V -only VHR file load" +
      s" \n ***************Job exit -Rerun job with given parameter****************************")
    sys.exit()
  }
  var v_path = args(0) //"2021-12-01"
  var v_year = args(1) //"2021-12-01"
  var v_start_week_no = args(2).toInt // "2022-01-31"
  var v_end_week_no = args(3).toInt //
  var rerun_job = args(4)
  println(s" Complete path of raw folder : $v_path/$v_year/{W$v_start_week_no-W$v_end_week_no}/Demography_Statements/Weekly_Final_WO_TBR/")
  var sub = s"Job started for data loading @ ${LocalDateTime.now()} "
  var   p_start_week_no=v_start_week_no
  val no_of_weeks = v_end_week_no.toInt - v_start_week_no.toInt
  var cont = s"<html><body>" +
    s" <br>Hi All,<br>" +
    s"<br> Job Argument details:" +
    s" <br>Week Start no  : $v_start_week_no" +
    s" <br>Week End no   : $v_end_week_no" +
    s" <br>Year : $v_year " +
    s" <br>Rerun_job  : $rerun_job" +
    s" <br>Complete path of raw folder : $v_path/$v_year/{W$v_start_week_no-W$v_end_week_no}/Demography_Statements/Weekly_Final_WO_TBR/" +
    s" <br>JOb will run for total no of weeks :$no_of_weeks" +
    s" <br><br><br>Thanks," +
    s"<br> Big data Team" +
    s"<body><html>"
  sendMail(sub, cont)
  var st_date = ""
  var end_date = ""
  var msg =""
  try {
    if (rerun_job == "A") {
      raise_Info("Bootstrap_processing", "INFO", "75", s"[$rerun_job][hh_vhr_generator] Days:   \n =======Truncating intermediate tables =======")

      spark.sql("Drop table if exists  df_union1")
      spark.sql("Drop table if exists df_union2")
      spark.sql("Drop table if exists df_union3")

      raise_Info("Bootstrap_processing", "INFO", "83", s"[$rerun_job][hh_vhr_generator] Days:   \n =======Starting Week wise Loading of csv files =======")

      for (w <- 0 to no_of_weeks) {
        var demo_dir = s"$v_path/$v_year/W$v_start_week_no/Demography_Statements/Weekly_Final_WO_TBR/Demo"
        var files = getListOfFiles(new File(demo_dir), List("csv"))
        println("No of file :", files.length)
        raise_Info("Bootstrap_processing", "INFO", "89", s"[$rerun_job][hh_vhr_generator] : $demo_dir  \n =======Starting Week wise loop :$v_start_week_no \n No.of files : ${files.length}=======")
        for (file <- files)
        {
          println("Processing for file name :", file)
          var date_1 = file.getName.substring(0, 8)
          var recursiveLoadedDF = spark.read
            .format("csv")
            .option("header", "true")
            .option("delimiter", ",")
            .option("inferSchema", "true")
            .load(s"file://$file")
          recursiveLoadedDF = recursiveLoadedDF.drop("Years_of_experience")
          recursiveLoadedDF = recursiveLoadedDF.withColumnRenamed("Ns_segment1","AM_segment2")
          recursiveLoadedDF = recursiveLoadedDF.withColumnRenamed("individual_id", "individual")
          recursiveLoadedDF = recursiveLoadedDF.withColumn("week", lit(s"W$v_start_week_no"))
          recursiveLoadedDF = recursiveLoadedDF.withColumn("date_1", lit(date_1))
          recursiveLoadedDF.coalesce(1).write.mode("append").saveAsTable("df_union2")
          raise_Info("Bootstrap_processing", "INFO", "113", s"[$rerun_job][hh_vhr_generator] Loaded Demo :  Week no.$v_start_week_no =======")
        }
        raise_Info("Bootstrap_processing", "INFO", "119", s"[$rerun_job][hh_vhr_generator] =======Loading Statement for  loop W$v_start_week_no =======")

        var statement_dir = s"$v_path/$v_year/W$v_start_week_no/Demography_Statements/Weekly_Final_WO_TBR/Statement"
        var files_stmt = getListOfFiles(new File(statement_dir), List("csv"))
        for (file <- files_stmt) {
          println("Processing for file name :", file)
          var date_1 = file.getName.substring(0, 8)
          var daily_df3 = spark.read
            .format("csv")
            .option("header", "true")
            .option("delimiter", ",")
            .option("inferSchema", "true")
            .load(s"file://$file")
          raise_Info("Bootstrap_processing", "INFO", "134", s"[$rerun_job][hh_vhr_generator]  ======= Loaded Statement for  loop W$v_start_week_no : ${date_1}_statement.csv======= ")
          daily_df3 = daily_df3.withColumnRenamed("individual_id", "individual")
          daily_df3 = daily_df3.withColumn("week", lit(s"W$v_start_week_no"))
          daily_df3 = daily_df3.withColumn("date_1", lit(date_1))
          daily_df3.coalesce(1).write.mode("append").saveAsTable("df_union3")
        }
        v_start_week_no = v_start_week_no.toInt + 1
      }
    }

  raise_Info("Bootstrap_processing", "INFO", "145", s"[$rerun_job][hh_vhr_generator] \n ======= End of Statement loop======= ")

  var hive_table = spark.sql("SELECT * FROM df_union2")
  var hive_table_unique = hive_table.coalesce(1).dropDuplicates("HOUSEHOLD")
  val unique_hh = hive_table_unique.select("HOUSEHOLD")
  unique_hh.write.mode(org.apache.spark.sql.SaveMode.Append).saveAsTable("UniqueHH")
  raise_Info("Bootstrap_processing", "INFO", "116", s"[$rerun_job][hh_vhr_generator]  \n ======= Created & saving table UniqueHH ======= ")

  val date_Ext = spark.sql("select max(date_1) end_date,min(date_1) start_date from df_union3")

  var st_date = date_Ext.head().get(1)
  var end_date = date_Ext.head().get(0)
  raise_Info("Bootstrap_processing", "INFO", "155", s"[$rerun_job][hh_vhr_generator]  =======Start date:$st_date End date:$end_date ======= ")

  /////////extra or necessary that is need to be cleared --- for demo
  var hive_table2 = spark.sql("SELECT * FROM df_union2")

  raise_Info("Bootstrap_processing", "INFO", "177", s"[$rerun_job][hh_vhr_generator]  ======= table created appended_csv_data2 ======= ")

  var univ_joined_df = unique_hh.join(hive_table2, Seq("HOUSEHOLD"), "inner") //(hive_table2, unique_hh("HOUSEHOLD") === hive_table2("HOUSEHOLD"), "inner")

  univ_joined_df.write.mode("overwrite").saveAsTable(s"${st_date}_${end_date}_demo")
  raise_Info("Bootstrap_processing", "INFO", "185", s"[$rerun_job][hh_vhr_generator]  ======= table created " + st_date + "_" + end_date + s"_demo.csv : ${univ_joined_df.count()} ======= ")

  /////////extra or necessary that is need to be cleared --- for stat
  var hive_table3 = spark.sql("SELECT * FROM df_union3")
  hive_table3.createOrReplaceTempView("appended_csv_data3")

  var univ_joined_df3 = unique_hh.join(hive_table3, Seq("HOUSEHOLD"), "inner") //(hive_table2, unique_hh("HOUSEHOLD") === hive_table2("HOUSEHOLD"), "inner")
  univ_joined_df3.write.mode("overwrite").saveAsTable(s"${st_date}_${end_date}_stat")
  raise_Info("Bootstrap_processing", "INFO", "197", s"[$rerun_job][hh_vhr_generator]  ======= table created $st_date _$end_date _stat : ${univ_joined_df3.count()}  ======= ")

  if (rerun_job == "A" || rerun_job == "H") {
    var demo = spark.sql(s"select * from ${st_date}_${end_date}_demo ")
    demo.write.mode("overwrite").saveAsTable("demo")
    //  demo.createOrReplaceTempView("demo")

    raise_Info("Bootstrap_processing", "INFO", "215", s"[$rerun_job][hh_vhr_generator]  =======temp table created demo ======= ")

    var demo1 = demo.groupBy("HOUSEHOLD", "date_1", "individual", "WEIGHT", "state_group", "detailed_town_class", "Detailed_NCCS", "Sex", "Age_in_years", "Education", "Mother_Tongue", "week").count()
    var demo2 = demo1.select("HOUSEHOLD", "individual", "date_1", "Age_in_years")
    //demo2.createOrReplaceTempView("demo2")
    demo2.write.mode("overwrite").saveAsTable("demo2")
    raise_Info("Bootstrap_processing", "INFO", "221", s"[$rerun_job][hh_vhr_generator]  =======temp table created demo2 ======= ")

    var distinct_age = spark.sql("select distinct HOUSEHOLD,individual,date_1,Age_in_years from demo2 order by HOUSEHOLD,date_1 desc, individual")
    distinct_age.createOrReplaceTempView("distinct_age")
    raise_Info("Bootstrap_processing", "INFO", "225", s"[$rerun_job][hh_vhr_generator]  =======temp table created distinct_age ======= ")
    // println("distinct_age table count ",distinct_age.count())
    var distinct_age1 = spark.sql("select * from ( select   HOUSEHOLD,individual,date_1,Age_in_years,row_number() over( partition by HOUSEHOLD,individual order by 1,2 ) as row_num from distinct_age) where row_num = 1")
    //distinct_age1.createOrReplaceTempView("distinct_age1")
    distinct_age1.write.mode("overwrite").saveAsTable("distinct_age1")
    raise_Info("Bootstrap_processing", "INFO", "229", s"[$rerun_job][hh_vhr_generator]  =======temp table created distinct_age1 ======= ")
    println("distinct_age1 table count ", distinct_age1.count())
    var demo22 = demo1.select("HOUSEHOLD", "individual", "date_1", "Sex")
    demo22.createOrReplaceTempView("demo22")
    raise_Info("Bootstrap_processing", "INFO", "233", s"[$rerun_job][hh_vhr_generator]  =======temp table created demo22 ======= ")
    // println("demo22 table count ",demo22.count())
    var distinct_sex = spark.sql("select distinct HOUSEHOLD,individual,date_1,Sex from demo22 group by HOUSEHOLD,individual,date_1,Sex order by HOUSEHOLD,date_1 desc, individual")
    distinct_sex.createOrReplaceTempView("distinct_sex")
    raise_Info("Bootstrap_processing", "INFO", "237", s"[$rerun_job][hh_vhr_generator]  =======temp table created distinct_sex ======= ")
    println("distinct_sex table count ", distinct_sex.count())
    val distinct_sex1 = distinct_sex.select(col("HOUSEHOLD"), col("individual"), col("date_1"), col("Sex"), row_number().over(Window.partitionBy(col("HOUSEHOLD"), col("individual")).orderBy(col("HOUSEHOLD"), col("date_1") desc, col("individual"))).alias("Row_Num")).distinct()
    // distinct_sex1.createOrReplaceTempView("distinct_sex1")
    distinct_sex1.write.mode("overwrite").saveAsTable("distinct_sex1")
    raise_Info("Bootstrap_processing", "INFO", "241", s"[$rerun_job][hh_vhr_generator]  =======temp table created distinct_sex1 ======= ")
    println("distinct_sex1 table count ", distinct_sex1.count())
    /*demo1 = spark.sql("select c.week, c.HOUSEHOLD household_id, c.date_1 Date,c.individual member_ID, c.WEIGHT Weights, c.state_group STATE, c.detailed_town_class TownVillageCode, " +
    "c.Detailed_NCCS NCCS, c.Education, c.Mother_Tongue Language, round(c.Age_in_years) Age,d.Sex Gender " +
    "from (select a.* from" +
    "        (select HOUSEHOLD, date_1, individual, WEIGHT, state_group, detailed_town_class, Detailed_NCCS, Sex, Age_in_years, Education, Mother_Tongue, week " +
    "          from demo ) a " +
    "left join distinct_age1 b " +
    "on a.HOUSEHOLD=b.HOUSEHOLD and a.individual=b.individual) c " +
    "left join (select distinct HOUSEHOLD,individual,Sex  from distinct_sex1) d " +
    "on c.HOUSEHOLD=d.HOUSEHOLD and c.individual=d.individual").toDF()*/

    //from_unixtime(unix_timestamp(a.date_1, 'yyyyMMdd'), 'yyyyMMdd')
    demo1 = spark.sql(
      s"""
         |select a.week, a.HOUSEHOLD household_id,a.date_1  date,a.individual member_ID, a.WEIGHT Weights, a.state_group STATE,
         |a.detailed_town_class TownVillageCode,
         |a.Detailed_NCCS NCCS, a.Education, a.Mother_Tongue Language, round(a.Age_in_years) Age,d.Sex Gender
         |from
         |(select HOUSEHOLD, date_1, individual, WEIGHT, state_group, detailed_town_class, Detailed_NCCS, Sex, Age_in_years, Education, Mother_Tongue, week
         | from demo ) a
         |left join distinct_age1 b on a.HOUSEHOLD=b.HOUSEHOLD and a.individual=b.individual
         |left join (select distinct HOUSEHOLD,individual,Sex  from distinct_sex1) d
         |on a.HOUSEHOLD=d.HOUSEHOLD and a.individual=d.individual
         |and b.HOUSEHOLD=d.HOUSEHOLD and b.individual=d.individual
         | and a.date_1 is not null
         |""".stripMargin)
   // println(" 270 : demo1 table count ", demo1.count())

    // var demo_final = demo1.select("week", "Date", "household_id", "member_ID", "Gender", "Age", "STATE", "TownVillageCode", "NCCS", "Weights", "Language", "Education")
    //demo_final.createOrReplaceTempView("demo_final")

    raise_Info("Bootstrap_processing", "INFO", "254", s"[$rerun_job][hh_vhr_generator]  =======temp table created demo_final ======= ")
    //demo_final = demo_final.withColumnRenamed("Date", "date")
    raise_Info("Bootstrap_processing", "INFO", "256", s"[$rerun_job][hh_vhr_generator]  ======= HH member formatting data column ======= ")

    //demo_final = demo_final.withColumn("date", to_date(col("date"), "yyyy-mm-dd")) //"dd-mm-yyyy"))
    var demo_final = demo1.select("household_id", "member_ID", "Gender", "Age", "STATE", "TownVillageCode", "NCCS", "Weights", "Language", "Education", "date", "week")
    raise_Info("Bootstrap_processing", "INFO", "260", s"[$rerun_job][hh_vhr_generator]  ======= HH member filtering memberid >0 ======= ")
    demo_final.write.mode("overwrite").saveAsTable("demo_final")

    //var frame_filtered = demo_final.filter(col("member_ID") > 0)
    var frame_filtered = spark.sql("select household_id,member_id,gender,age, state,townvillagecode, nccs,weights," +
      " language,education,from_unixtime(unix_timestamp(date, 'yyyyMMdd'), 'yyyy-MM-dd')date,week " +
      " from demo_final " +
      " where  member_ID>0 and week is not null")
    raise_Info("Bootstrap_processing", "INFO", "263", s"[$rerun_job][hh_vhr_generator]  ======= HH member saving to file ======= ")

   /* frame_filtered.coalesce(1).write
      .format("com.databricks.spark.csv")
      .option("header", "true")
      .mode("overwrite")
      .save(s"./Inputs/HH_Member_Table_$st_date" + "_" + s"$end_date" + ".csv")*/
    frame_filtered.coalesce(1).write.mode("overwrite").saveAsTable(s"HH_Member_Table_${v_year}_W${p_start_week_no}_W$v_end_week_no")
    raise_Info("Bootstrap_processing", "INFO", "271", s"[$rerun_job][hh_vhr_generator]  ======= file created HH_Member_Table_${v_year}_W${p_start_week_no}_W$v_end_week_no ======= ")

    /////--------------------------------------HH_member_table
  }

  if (rerun_job == "A" || rerun_job == "V") {
    /////---------------for getting statement file
    raise_Info("Bootstrap_processing", "INFO", "290", s"[$rerun_job][hh_vhr_generator]  ======= Reading $st_date" + "_" + s"$end_date" + "_stat.csv/======= ")


    var statement = spark.sql(s"select * from  ${st_date}_${end_date}_stat where ooh=0")
    // statement = statement.filter(col("ooh") === 0)
    //statement.createOrReplaceTempView("statement")
    statement.write.mode("overwrite").saveAsTable("statement")
    raise_Info("Bootstrap_processing", "INFO", "292", s"[$rerun_job][hh_vhr_generator]  ======= temp table created statement======= ")

    var statement_with_demo_tmp = spark.sql("select a.*,b.Date,b.household_id,b.member_ID,b.Gender,b.Age ,b.STATE State_group," +
      "b.TownVillageCode,b.NCCS,b.Weights,b.Language,b.Education " +
      " from statement a left join demo_final b on a.HOUSEHOLD=b.household_id and a.INDIVIDUAL=b.member_ID" +
      " and a.date_1=b.Date")

    println("statement_with_demo_tmp:", statement_with_demo_tmp.count())
    statement_with_demo_tmp.write.mode("overwrite").saveAsTable("statement_with_demo_tmp")
    var demo11 = spark.sql("select HOUSEHOLD,individual,date_1,Mode_of_reception,TV_Signal_Provider,census_state_code,count(*) n from demo group by HOUSEHOLD,individual,date_1,Mode_of_reception,TV_Signal_Provider,census_state_code")
    demo11.write.mode("overwrite").saveAsTable("demo11")
    //statement_with_demo.createOrReplaceTempView("statement_with_demo")

    //demo11.createOrReplaceTempView("demo11")

    raise_Info("Bootstrap_processing", "INFO", "294", s"[$rerun_job][hh_vhr_generator]  ======= temp table created statement_with_demo_tmp======= ")

    // statement_with_demo = statement_with_demo.withColumn("end", lit(col("begin") + col("DURATION"))) //statement_with_demo$begin + statement_with_demo$DURATION <- statement_with_demo$begin + statement_with_demo$DURATION
    var stg = spark.read
      .format("csv")
      .option("header", "true")
      .option("delimiter", ",")
      .option("inferSchema", "true")
      .load(s"./Inputs/stategroup.csv").toDF()
    stg.createOrReplaceTempView("stg")
    var query =
      s"""
         |select a.HOUSEHOLD household_id,a.INDIVIDUAL member_id,a.START begin,a.DURATION,(a.START + a.DURATION) end,a.CHANNEL session_id,a.TVSET tvset_id,a.PLATFORM,a.ooh,a.week,a.date_1 date,
         |a.Gender sex,a.Age age_in_years,a.TownVillageCode townVillageCode ,a.NCCS detailed_nccs,a.Weights weight,a.Language,a.Education education,b.Mode_of_reception mode_of_reception,b.TV_Signal_Provider tv_signal_provider,b.census_state_code,b.n,a.State_group
         |from statement_with_demo_tmp a
         |left join demo11 b
         |on a.HOUSEHOLD=b.HOUSEHOLD and a.INDIVIDUAL=b.INDIVIDUAL and a.date_1=b.date_1
         |""".stripMargin
    println(query)
    var statement_with_demo = spark.sql(query)
    //statement_with_demo.createOrReplaceTempView("statement_with_demo")
    println("statement_with_demo:", statement_with_demo.count())
    statement_with_demo.write.mode("overwrite").saveAsTable(s"statement_with_demo")

    raise_Info("Bootstrap_processing", "INFO", "314", s"[$rerun_job][hh_vhr_generator]  =======  table created statement_with_demo======= ")
    var cntr = 0

    spark.sql(s"drop table if exists VHR_Table_${v_year}_W${p_start_week_no}_W$v_end_week_no")
    stg.select("*").orderBy("state_group").collect().foreach { data =>
      try {
        var state_group = data.get(0)
        var state = data.get(1)
        raise_Info("Bootstrap_processing", "INFO", "327", s"[$rerun_job][hh_vhr_generator]  ======= selecting data for $state_group & $state ======= ")

        var statement_with_demo_final = spark.sql(s"" +
          s" select  household_id, member_id, tvset_id,  session_id, begin, end, weight, " +
          s" '$state_group' state_group, census_state_code , townVillageCode detailed_town_class, detailed_nccs, age_in_years, sex, education, mode_of_reception," +
          s" tv_signal_provider, '$state' state,from_unixtime(unix_timestamp(b.date, 'yyyyMMdd'), 'yyyy-MM-dd') date, week" +
          s" from statement_with_demo b " +
          s" where State_group=$state_group")
        raise_Info("Bootstrap_processing", "INFO", "327", s"[$rerun_job][hh_vhr_generator]  ======= Writing to VHR_Table_${v_year}_W${p_start_week_no}_W$v_end_week_no for $state_group & $state ======= ")

        statement_with_demo_final.write.partitionBy( "state").mode("append").saveAsTable(s"VHR_Table_${v_year}_W${p_start_week_no}_W$v_end_week_no")

        cntr = cntr + 1
        raise_Info("Bootstrap_processing", "INFO", "327", s"[$rerun_job][hh_vhr_generator]  ======= Saved  to VHR_Table_${v_year}_W${p_start_week_no}_W$v_end_week_no for $state_group======= ")
      }
      catch {
        case ex: Exception => println(ex);
      }
    }
    /////--------------------------------------VHR_table
   msg=  s" File has been created successfully<br>" +
     s" Job start time  $job_start and end at ${LocalDateTime.now()}" +
     s" <br> HH Member Table name : HH_Member_Table_${v_year}_W${p_start_week_no}_W$v_end_week_no " +
     s" <br> VHR Table name :VHR_Table_${v_year}_W${p_start_week_no}_W$v_end_week_no "
  }
}
  catch
  {
    case ex: Exception => println(ex)
      msg=  s" File generation has been Failed <br>" +
        s" <br>Job start time  $job_start and end at ${LocalDateTime.now()}" +
        s" <br> Failed to load data . Due to below error" +
        s" <br>Error: $ex "
  }
    raise_Info("Bootstrap_processing", "INFO", "342", s"[$rerun_job][hh_vhr_generator]  ======= VHR table saved to csv ======= ")

    var subject =s"File loading status for Bootstrap Process "
    var content = s"Hi All,<br>" +
      s"<br>******************************************<br>" +
      s" $msg" +
      s"<br>******************************************" +
      s" <br>Thanks," +
      s" <br> Big data Team" +
      s"<br><br><br> Note: In case of any inconsistency of result kindly check the log files " +
      s"<br>*** This is an automatically system generated email. *** "


    sendMail(subject,content)
    raise_Info("Bootstrap_processing", "INFO", "344", s"[$rerun_job][hh_vhr_generator]  ======= End of file creation process ======= ")


  def getListOfFiles(dir: File, extensions: List[String]): List[File] = {
    dir.listFiles.filter(_.isFile).toList.filter { file =>
      extensions.exists(file.getName.endsWith(_))
    }
  }
}



