package com.barc.main
import scala.io.Source
import scala.util.{Failure, Success, Try}
import org.apache.spark.sql.SparkSession
import com.barc.common.raise_Info

import org.apache.log4j.Logger
import org.apache.log4j.Level

import java.util.Date
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

import org.apache.spark.sql.{DataFrame, SparkSession}
object mainApi extends App {
/******************Start of Code*************/

  // Spark Session Initialization
  //print(spark)

  Logger.getLogger("org").setLevel(Level.OFF)
  Logger.getLogger("akka").setLevel(Level.OFF)
  Logger.getLogger("info").setLevel(Level.OFF)

  raise_Info("mainApi","INFO","20","***************Start of Processing*********************")

  val spark = SparkSession.builder().master("local").appName("BootstrapAPI").enableHiveSupport().getOrCreate()
  spark.sparkContext.setLogLevel("ERROR")
  // val fileConfig = args(0)

  println(spark)
  var load_df = spark.read.option("header", "true").option("inferSchema", "true").csv(s"C:\\Users\\swaminath.morya\\Downloads\\random.csv")
  load_df.write.mode(org.apache.spark.sql.SaveMode.Overwrite).format("parquet").saveAsTable(s"random_tbl")

  def generate_series = udf((max_id:Int,min_id:Int) => {
    var df =spark.sql(s"select collect_set(($min_id+i))[Cast((FLOOR(RAND()*5)) as INT)] " +
      s" from (select posexplode(split(space($max_id-$min_id),' ')) as (i,x))seq ")
    df.head.get(0).toString.toInt
  })

  spark.udf.register("generate_series", generate_series)
 spark.sql("select generate_series(max_id,min_id) from random_tbl").show()


/************End of Code***********/
}
